//
//  AlertFactory.swift
//  TimeTracker
//
//  Created by aarthur on 3/1/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Foundation

typealias CompletionResponse = (NSApplication.ModalResponse) -> Void

struct AlertFactory {
    
    static func validationDialog(message: String, style: NSAlert.Style = .critical) {
        let alert = NSAlert()
        
        alert.messageText = message
        alert.addButton(withTitle: "OK")
        alert.alertStyle = style
        alert.runModal()
    }
    
    static func alertSheet(in window: NSWindow, message: String, affirmative: String, nonAffirmative: String, responseHandler: @escaping CompletionResponse) {
        let alert = NSAlert()
        
        alert.messageText = message
        alert.addButton(withTitle: affirmative)
        alert.addButton(withTitle: nonAffirmative)
        alert.alertStyle = .warning
        alert.beginSheetModal(for: window) { (response) in
            responseHandler(response)
        }
    }
}
