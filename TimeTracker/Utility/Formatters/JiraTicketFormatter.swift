//
//  JiraTicketFormatter.swift
//  TimeTracker
//
//  Created by aarthur on 2/21/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

//not used -- I had an idea but abandoned it for now
enum TicketType: String {
    case IT
    case PUL
    case none = ""
    
    func isPUL() -> Bool {
        switch self {
        case .IT:
            return false
        case .PUL:
            return true
        default:
            return false
        }
    }
    
    func prefix() -> TicketType {
        switch self {
        case .IT:
            return .IT
        case .PUL:
            return .PUL
        default:
            return .none
        }
    }
}

class JiraTicketFormatter: Formatter {

    override func string(for obj: Any?) -> String? {
        if let localString = obj as? String {
            if localString.hasPrefix("PUL-") || localString.hasPrefix("IT-") {
                return localString
            }
            else {
                if localString.count > 4 {
                    return "IT-\(localString)"
                }
                return "PUL-\(localString)"
            }
        }
        return obj as? String
    }
    
    override func getObjectValue(_ obj: AutoreleasingUnsafeMutablePointer<AnyObject?>?, for string: String, errorDescription error: AutoreleasingUnsafeMutablePointer<NSString?>?) -> Bool {
        obj?.pointee = string as AnyObject
        return true
    }
}
