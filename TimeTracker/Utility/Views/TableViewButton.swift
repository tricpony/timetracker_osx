//
//  TableViewButton.swift
//  TimeTracker
//
//  Created by aarthur on 2/17/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

class TableViewButton: NSButton {
    @IBOutlet var tableCellView: NSTableCellView!
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    lazy var representedObject: Any? = {
        if let safeObject = tableCellView.objectValue {
            return safeObject
        }
        return nil
    }()
    
}
