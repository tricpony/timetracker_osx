//
//  XMLParseWorker.swift
//  TimeTracker
//
//  Created by aarthur on 2/25/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Foundation

typealias ParseCompletion = ([String:Any]?, Error?) -> Void

enum ObjectType: String {
    case IDENTITY
    case DOCUMENT
    case CUSTOMER
    case JOB
    case SERVICE
    case TIMEPERIOD
    case WillChange
    case None
    
    func type(for att: [String:String]) -> ObjectType {
        if let argValue = att["type"] {
            if let type = ObjectType.init(rawValue: argValue) {
                return type
            }
            else {
                return .None
            }
        }
        return .None
    }
    
    func modelNameValue() -> String {
        
        switch self {
        case .IDENTITY:
            return "employeename"
        case .TIMEPERIOD:
            return "notes"
        case .CUSTOMER, .JOB, .SERVICE:
            return "name"
        default:
            return "who knows?"
        }
    }
    
    func imtIDKey() -> String {
        switch self {
        case .CUSTOMER, .JOB, .SERVICE, .IDENTITY, .TIMEPERIOD:
            return "id"
        default:
            return "who knows?"
        }
    }
    
    func jobKey() -> String {
        switch self {
        case .CUSTOMER:
            return "JOB"
        default:
            return ""
        }
    }
    
    func modelClassInstance(in ctx: NSManagedObjectContext) -> Importable? {
        switch self {
        case .IDENTITY:
            return Enterprise.mr_createEntity(in: ctx)
//        case .DOCUMENT:
//            return
        case .CUSTOMER:
            return Client.mr_createEntity(in: ctx)
        case .JOB:
            return StatementOfWork.mr_createEntity(in: ctx)
        case .SERVICE:
            return Project.mr_createEntity(in: ctx)
        default:
            return nil
        }
    }
}

enum AttributeName: String {
    case employeename
    case companyfilename
    case identity
    case name
    case WillChange
    case None
    
    func name(for att:[String:String], type: ObjectType) -> AttributeName {
        if type == .DOCUMENT { return .None }
        if let argValue = att["name"] {
            if let name = AttributeName(rawValue: argValue) {
                return name
            }
            else {
                return .None
            }
        }
        return .None
    }
    
}

enum BufferStatus {
    case open
    case closed
}

class XMLParseWorker: NSObject, XMLParserDelegate {
    var isParsing = false
    var buffer: String = ""
    var bufferStatus: BufferStatus = .closed
    var graph = [String:[Any]]()
    var error: Error? = nil
    let parseEngine: XMLParser!
    var completion: ParseCompletion?
    var currentObjectType: ObjectType = .None
    var currentAttributeName: AttributeName = .None
    var graphNode = [[String:String]]()
    
    init(xmlURL: URL) {
        self.parseEngine = XMLParser.init(contentsOf: xmlURL)
    }
    
    func startParsing(completion: @escaping ParseCompletion) {
        parseEngine.delegate = self
        self.completion = completion
        isParsing = true
        parseEngine.parse()
    }
    
    // MARK: - XMLParserDelegate
    
    func parser(_ parser: XMLParser,
                didStartElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?,
                attributes attributeDict: [String : String] = [:]) {
        if currentObjectType == .WillChange {
            currentObjectType = currentObjectType.type(for: attributeDict)
            if currentAttributeName == .None {
                guard let value = attributeDict["id"] else { return }
                let item = ["id":value]
                graphNode = [item]
            }
            
        }
        else {
            
            switch currentObjectType {
            case .None, .WillChange:
                return
            default:
                switch currentAttributeName {
                case .None, .WillChange:
                    currentAttributeName = currentAttributeName.name(for: attributeDict, type: currentObjectType)
                default:
                    return
                }
            }
            
            if (currentAttributeName == .None) {
                switch currentObjectType {
                case .DOCUMENT, .JOB, .CUSTOMER, .SERVICE:
                    if let value = attributeDict["destination"] {
                        guard let id = attributeDict["idrefs"] else { return }
                        if var item = graphNode.last {
                            let i = graphNode.count - 1
                            item[value] = id;
                            graphNode[i] = item
                        }
                    }
                default:
                    return
                }
            }

        }
    }
    
    func parser(_ parser: XMLParser,
                didEndElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?) {
        if elementName == "databaseInfo" {
            currentObjectType = .WillChange
        }
        else if elementName == "object" {
            if graph[currentObjectType.rawValue] == nil {
                graph[currentObjectType.rawValue] = graphNode
            }
            else {
                var items = graph[currentObjectType.rawValue] as! [[String:String]]
                items.append(graphNode.last!)
                graph[currentObjectType.rawValue] = items
            }
            currentObjectType = .WillChange
            currentAttributeName = .None
        }
        else if elementName == "attribute" {
            currentAttributeName = .WillChange
        }
        
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        switch currentAttributeName {
        case .None, .WillChange:
            return
        default:
            let cleanString = string.trim()
            if cleanString.count > 0 {
                if var item = graphNode.last {
                    let i = graphNode.count - 1
                    item[currentAttributeName.rawValue] = cleanString;
                    graphNode[i] = item
                }
            }
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        guard let safeCompletion = completion else { return }
        safeCompletion(graph, nil)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        guard let safeCompletion = completion else { return }
        safeCompletion(nil, parseError)
    }
}

