//
//  ImportWorker.swift
//  TimeTracker
//
//  Created by aarthur on 2/28/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Foundation

typealias ImportCompletion = (Error?) -> Void

struct ImportWorker {
    
     //content looks like:
     /**
     ([String : [Any]]) $R0 = 5 key/value pairs {
     [0] = {
     key = "IDENTITY"
     value = 1 value {
     [0] = 3 key/value pairs {
     [0] = (key = "id", value = "z102")
     [1] = (key = "employeename", value = "Gigabit, LLC")
     [2] = (key = "companyfilename", value = "/Users/david/Documents/Accounting/ZionSoftware 2.qb2016")
     }
     }
     }
     [1] = {
     key = "SERVICE"
     value = 2 values {
     [0] = 2 key/value pairs {
     [0] = (key = "id", value = "z170")
     [1] = (key = "name", value = "Consulting:Rally Remote - BArthur")
     }
     [1] = 2 key/value pairs {
     [0] = (key = "id", value = "z187")
     [1] = (key = "name", value = "Consulting:Rally Onsite - BArthur")
     }
     }
     }
     [2] = {
     key = "CUSTOMER"
     value = 1 value {
     [0] = 3 key/value pairs {
     [0] = (key = "id", value = "z121")
     [1] = (key = "JOB", value = "z134 z125 z195 z167")
     [2] = (key = "name", value = "Rally Health, Inc.")
     }
     }
     }
     [3] = {
     key = "JOB"
     value = 1 value {
     [0] = 3 key/value pairs {
     [0] = (key = "id", value = "z167")
     [1] = (key = "CUSTOMER", value = "z121")
     [2] = (key = "name", value = "SOW3 - Gigabit, LLC")
     }
     }
     }
     [4] = {
     key = "DOCUMENT"
     value = 1 value {
     [0] = 2 key/value pairs {
     [0] = (key = "id", value = "z103")
     [1] = (key = "IDENTITY", value = "z102")
     }
     }
     }
     }
     **/
    static func importUserTemplate(isLiveData: Bool, url: URL, ctx: NSManagedObjectContext, completion: @escaping ImportCompletion) {
        func errorMessage(objectType: String) -> String {
            return "Failed to create ObjectType for: \(objectType)"
        }
        func errorMessage(matching: String) -> String {
            return "Failed to create model object for: \(matching)"
        }
        func errorMessage(name: String) -> String {
            return "Failed to access name for: \(name)"
        }
        func errorMessage(startTime: String) -> String {
            return "Failed to access start time value for: \(startTime)"
        }
        
        let parser = XMLParseWorker(xmlURL: url)
        parser.startParsing { (content, error) in
            if error == nil {
                if let safeContent = content {
                    var enterprise: Importable!
                    var client: Importable!
                    var sow: Importable!
                    var projects = [Importable]()
                    var timeSlips = [Importable]()
                    var importPkg = [String : Any]()
                    for key in safeContent.keys {
                        guard let contentValue = safeContent[key] as? [[String:String]] else { return }
                        for nextValue in contentValue where key != "DOCUMENT" && (key != "ACTIVITY") {
                            if let objectType = ObjectType.init(rawValue: key) {
                                guard let modelObject = objectType.modelClassInstance(in: ctx) else { completion(makeError(why: errorMessage(matching: key))); return }
                                guard let name = nextValue[objectType.modelNameValue()] else { completion(makeError(why: errorMessage(name: key))); return }
                                guard let id = nextValue[objectType.imtIDKey()] else
                                { completion(makeError(why: "Failed to access import value id")); return }
                                modelObject.importableName = name
                                modelObject.importValue(["imtIdentifier": id])
                                
                                switch objectType {
                                case .IDENTITY:
                                    enterprise = modelObject
                                case .JOB:
                                    sow = modelObject
                                case .CUSTOMER:
                                    client = modelObject
                                    let configRel = CoreDataUtility.fetchConfigRelationship(in: ctx, configCless: "Client", templateSubstitution: "$JOB$")
                                    guard let jobs = nextValue[objectType.jobKey()] else
                                    { completion(makeError(why: "Failed to access job ids")); return }
                                    configRel?.cardinality = jobs

                                case .SERVICE:
                                    projects.append(modelObject as! Project)
                                case .TIMEPERIOD:
                                    guard let rawStartString = nextValue["start"] else { completion(makeError(why: errorMessage(startTime: key))); return }
                                    guard let rawStartValue = TimeInterval.init(rawStartString) else
                                    { completion(makeError(why: "Failed to create time interval from \(rawStartString)")); return }
                                    guard let rawDurationString = nextValue["duration"] else
                                    { completion(makeError(why: "Failed to access duration from \(key)")); return }
                                    guard let rawDurationValue = Float.init(rawDurationString) else
                                    { completion(makeError(why: "Failed to create duration from \(rawDurationString)")); return }
                                    let duration = rawDurationValue/3600
                                    
                                    (modelObject as! TimeSlip).startingDate = NSDate.dateWithTimeIntervalSince2000(interval: rawStartValue)
                                    (modelObject as! TimeSlip).duration = duration
                                    timeSlips.append(modelObject)
                                default:
                                    continue
                                }
                                let config = CoreDataUtility.fetchConfiguration(in: ctx, matchingObjectType: objectType.rawValue)
                                guard let imtSeedString = nextValue["id"] else { return }
                                if let imtSeed = Int32(imtSeedString.suffix(imtSeedString.count - 1)) {
                                    config?.nextIMT_ID = imtSeed
                                }
                            }
                            else {
                                completion(makeError(why: errorMessage(objectType: key)))
                            }
                        }
                    }
                    importPkg["Enterprise"] = enterprise
                    importPkg["Client"] = client
                    importPkg["StatementOfWork"] = sow
                    importPkg["timeslips"] = timeSlips
                    for nextProject in projects {
                        nextProject.assemble(importPkg, isLive: isLiveData)
                    }
                    ctx.mr_saveToPersistentStoreAndWait()
                    completion(error)
                }
            }
            else {
                completion(error)
            }
        }
    }
    
    static func makeError(why: String) -> NSError {
        let userInfo = [NSLocalizedDescriptionKey: why, NSLocalizedFailureReasonErrorKey: why]
        let error = NSError.init(domain: "XML", code: 112, userInfo: userInfo)
        return error
    }
    
}
