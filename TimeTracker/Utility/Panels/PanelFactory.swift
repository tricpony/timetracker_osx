//
//  PanelFactory.swift
//  TimeTracker
//
//  Created by aarthur on 2/25/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Foundation

typealias ModalResponse = (NSApplication.ModalResponse, URL?) -> Void

struct PanelFactory {
    static func openSavePanel(for window: NSWindow, completion: @escaping ModalResponse) {
        let savePanel = NSSavePanel()
        savePanel.canCreateDirectories = true
        savePanel.beginSheetModal(for: window) { (response) in
            completion(response, savePanel.url)
        }
    }
    
    static func openFindPanel(for window: NSWindow, completion: @escaping ModalResponse) {
        let openPanel = NSOpenPanel()
        openPanel.beginSheetModal(for: window) { (response) in
            completion(response, openPanel.url)
        }
    }
}
