//
//  ConfigurationUtility.swift
//  TimeTracker
//
//  Created by aarthur on 2/19/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Foundation

struct ConfigurationUtility {
    private static func configuration() -> [String:Any] {
        let bundle = Bundle.main
        if let path = bundle.path(forResource: "Configuration", ofType: "plist") {
            let url = URL(fileURLWithPath: path)
            let data = try! Data(contentsOf: url);
            let plist = try! PropertyListSerialization.propertyList(from: data, options: .mutableContainers, format: nil)
            return plist as! [String : Any]
        }
        return ["":""]
    }
    
    static func asJSON(_ of: [String:Any]) -> Data? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: of , options: .prettyPrinted)
            return jsonData
        } catch {
            print(error);
        }
        return nil
    }
    
    static func configurationAsJSON() -> Data? {
        return asJSON(configuration())
    }

    static func configurationVersion() -> String {
        if let version = configuration()["Version"] {
            return version as! String
        }
        return ""
    }
    
    static func configurationHeader() -> String {
        if let header = configuration()["header"] {
            return header as! String
        }
        return ""
    }
    
    static func configurationSettings() -> [[String:Any]] {
        if let settings = configuration()["IMT_Rules"] {
            return settings as! [[String : Any]]
        }
        return [["":""]]
    }
    
    static func configurationData() -> Data? {
        let data = Data(configurationSettings().description.utf8)
        return data
    }
    
    static func configurationRelationshipData(in parent: [String:Any]) -> Data? {
        if let configRelationships = parent["imt_relationships"] as? [[String:Any]] {
            let data = Data(configRelationships.description.utf8)
            return data
        }
        return nil
    }
}
