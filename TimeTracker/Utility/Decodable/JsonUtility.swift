//
//  JsonUtility.swift
//  CityLab
//
//  Created by aarthur on 11/9/18.
//  Copyright © 2018 Gigabit LLC. All rights reserved.
//

import Foundation

struct JsonUtility<T: Decodable> {
    
    static func parseJSON(_ payload: Data?, ctx: NSManagedObjectContext? = nil) -> T? {
        guard payload != nil else { return nil }

        let decoder = JSONDecoder()
        if let safeCtx = ctx {
            guard let codingUserInfoContextKey = CodingUserInfoKey.context else{
                fatalError()
            }
            decoder.userInfo[codingUserInfoContextKey] = safeCtx
        }
        
        do {
            let decoded = try decoder.decode(T.self, from: payload!)
            return decoded
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    static func parsePLIST(_ payload: Data?, ctx: NSManagedObjectContext? = nil) -> T? {
        guard payload != nil else { return nil }
        
        let decoder = PropertyListDecoder()
        if let safeCtx = ctx {
            guard let codingUserInfoContextKey = CodingUserInfoKey.context else{
                fatalError()
            }
            decoder.userInfo[codingUserInfoContextKey] = safeCtx
        }
        
        do {
            let decoded = try decoder.decode(T.self, from: payload!)
            return decoded
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
}
