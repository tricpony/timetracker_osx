//
//  NSObject+TimeTracker.swift
//  TimeTracker
//
//  Created by aarthur on 2/21/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Foundation

extension NSObject {

    class func className() -> String {
        let className = NSStringFromClass(type(of: self) as! AnyClass).components(separatedBy: ".").last!
        return className
    }
    
    
    func className() -> String {
        let className = NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
        return className
    }
}
