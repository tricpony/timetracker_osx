//
//  String+TimeTracker.swift
//  TimeTracker
//
//  Created by aarthur on 2/19/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Foundation

extension String {

    func findAndReplacement(of: [String:String]) -> String {
        var buffer = self
        for nextKey in of.keys {
            if let value = of[nextKey] {
                buffer = buffer.replacingOccurrences(of: nextKey, with: value)
            }
        }
        return buffer
    }

    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
    }
    
    func subString(to range: Range<String.Index>) -> String {
        let startIndex = range.upperBound
        let endIndex = range.lowerBound
        let substring = String(self[startIndex..<endIndex])
        return substring
    }
    
    func toPointer() -> UnsafePointer<UInt8>? {
        guard let data = self.data(using: String.Encoding.utf8) else { return nil }
        
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: data.count)
        let stream = OutputStream(toBuffer: buffer, capacity: data.count)
        
        stream.open()
        data.withUnsafeBytes({ (p: UnsafePointer<UInt8>) -> Void in
            stream.write(p, maxLength: data.count)
        })
        
        stream.close()
        
        return UnsafePointer<UInt8>(buffer)
    }
    
    func toRawPointer() -> UnsafeRawPointer? {
        return UnsafeRawPointer.init(toPointer())
    }

    func xmlEscaped() -> String {
        return replacingOccurrences(of: "&", with: "&amp;")
            .replacingOccurrences(of: "\"", with: "&quot;")
            .replacingOccurrences(of: "'", with: "&#39;")
            .replacingOccurrences(of: ">", with: "&gt;")
            .replacingOccurrences(of: "<", with: "&lt;")
    }
    
    func replaceNewLines(with: String = " ") -> String {
        return replacingOccurrences(of: "\n", with: with)
    }
    
    func range(of this: String) -> NSRange? {
        if this.count > self.count { return nil }
        
        var regEx = ""
        var cr = NSRange()

        regEx = String(format: "\\s+%@\\s=", this)
        var r = self.range(of: regEx, options: .regularExpression, range: nil, locale: nil)
        if r?.lowerBound == r?.upperBound {
            r = self.range(of: this, options: .literal, range: nil, locale: nil)
            cr = this.nsRange(from: r!)
        }
        else {
            let matchingString = self.subString(to: r!)
            let scanner = Scanner.init(string: matchingString)
            scanner.charactersToBeSkipped = nil
            if scanner.scanUpTo(String(this.prefix(1)), into: nil) {
                cr.location = scanner.scanLocation
                cr.length = this.count
            }
            else {
                return nil
            }
        }
        return cr
    }
    
    func stringBetween(leading: String, trailing: String) -> String {
        var resultRange = NSRange(location: 0, length: 0)
        let scanner = Scanner(string: self)
        
        if scanner.scanUpTo(leading, into: nil) {
            resultRange.location = scanner.scanLocation
            if scanner.scanUpTo(trailing, into: nil) {
                resultRange.length = scanner.scanLocation - resultRange.location
            }
        }
        let startIndex = self.index(self.startIndex, offsetBy: resultRange.location)
        let endIndex = self.index(self.startIndex, offsetBy: scanner.scanLocation)

        return String(self[startIndex..<endIndex])
    }
}

extension StringProtocol where Index == String.Index {
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
}

