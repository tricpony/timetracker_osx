//
//  Date+TimeTracker.swift
//  TimeTracker
//
//  Created by aarthur on 2/10/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Foundation

extension NSDate {
    enum WeekDay: Int {
        case sunday = 1
        case monday
        case tuesday
        case wednesday
        case thursday
        case friday
        case saturday
    }
    
    func getWeekDay() -> WeekDay {
        let calendar = Calendar.current
        let weekDay = calendar.component(Calendar.Component.weekday, from: self as Date)
        return WeekDay(rawValue: weekDay)!
    }

    func isWeekend() -> Bool {
        switch getWeekDay() {
        case .sunday, .saturday:
            return true
        default:
            return false
        }
    }

    class func dateOn(year: Int, month: Int, day: Int, hour: Int = 0, minute: Int = 0, second: Int = 0) -> Date? {
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = hour
        dateComponents.minute = minute
        dateComponents.second = second
        
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents)
        return date;
    }
    
    class func dateWithTimeIntervalSince2000(interval: TimeInterval) -> NSDate? {
        if let century = NSDate.dateOn(year: 2000, month: 1, day: 1) {
            let date = NSDate.init(timeInterval: interval, since: century)
            return date
        }
        return nil
    }
    
    func startOfMonth() -> Date? {
        guard let interval = Calendar.current.dateInterval(of: .month, for: self as Date) else { return nil }
        return interval.start
    }
    
    func endOfMonth() -> Date? {
        guard let start = startOfMonth() else { return nil }
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: start)!
    }

    func dateByAdding(days: Int) -> NSDate? {
        var dateComponents = DateComponents()
        let calendar = Calendar.current

        dateComponents.day = days
        return calendar.date(byAdding: dateComponents, to: self as Date) as NSDate?
    }
    
    func weekDayCountOfMonthSoFar() -> Int {
        var count = 0;
        guard let firstDayOfMonth = startOfMonth() as NSDate? else { return 0 }
        var priorDay = self
        while priorDay.isGreaterThan(firstDayOfMonth) {
            count += priorDay.isWeekend() ? 0 : 1
            guard let safeDate = priorDay.dateByAdding(days: -1) else { break }
            priorDay = safeDate
        }
        return count
    }
    
    func isInCurrentMonth(date: NSDate) -> Bool {
        guard let firstDayOfMonth = startOfMonth() as NSDate? else { return false }
        guard let lastDayOfMonth = endOfMonth() as NSDate? else { return false }
        return date.isGreaterThanOrEqual(to:firstDayOfMonth) && date.isLessThanOrEqual(to:lastDayOfMonth)
    }

    func asString(formatter: DateFormatter) -> String {
        return formatter.string(from: self as Date)
    }
    
    func timeIntervalSince2000() -> TimeInterval {
        if let century = NSDate.dateOn(year: 2000, month: 1, day: 1) {
            return (century as NSDate).timeIntervalSince(self as Date)
        }
        return 0
    }
}
