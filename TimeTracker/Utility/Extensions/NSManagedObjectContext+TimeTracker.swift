//
//  NSManagedObjectContext+TimeTracker.swift
//  TimeTracker
//
//  Created by aarthur on 2/24/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Foundation

extension NSManagedObjectContext {
    
    func persistanceStoreURL() -> URL? {
        if let coord = self.persistentStoreCoordinator {
            let url = coord.url(for: coord.persistentStores.first!)
            return url
        }
        return nil
    }

}
