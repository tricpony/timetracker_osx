//
//  EnterpriseViewController.swift
//  TimeTracker
//
//  Created by aarthur on 2/3/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

class EnterpriseViewController: BaseViewController {
    @IBOutlet weak var companyNameTextField: NSTextField!
    @IBOutlet weak var logoImageWell: NSImageView!
    @IBOutlet var objectController: NSObjectController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let safeMe = Enterprise.enterpriseObject(in: managedObjectContext) {
            objectController.addObject(safeMe)
        }
    }
    
    override func windowFrameAutoSaveName() -> String {
        return "TimeTracker_Enterprise"
    }

    @IBAction func saveDocument(_ sender: Any) {
        guard companyNameTextField.stringValue.count > 0 else {
            validationDialog(message: "Please enter company name.")
            return
        }
        if let safeWindow = self.view.window {
            view.window?.endEditing(for: self)
            safeWindow.sheetParent?.endSheet(safeWindow, returnCode: .OK)
        }
    }
}
