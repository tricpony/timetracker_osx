//
//  TimeSlipViewController.swift
//  TimeTracker
//
//  Created by aarthur on 2/11/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

@objc protocol PopupButtonBindingKeyValueCompliance:class {
    var statusChoices: [[String:String]] { get }
    var rowHeight: CGFloat { get }
}

class TimeSlipViewController: BaseViewController, PopupButtonBindingKeyValueCompliance {
    @IBOutlet var timeSlipObjectController: NSObjectController!
    @IBOutlet var workSlipArrayController: NSArrayController!
    @IBOutlet var codeReviewArrayController: NSArrayController!
    @IBOutlet weak var durationField: NSTextField!
    @IBOutlet weak var outerSplitView: NSSplitView!
    @IBOutlet weak var innerSplitView: NSSplitView!
    @IBOutlet var notesTextView: NSTextView!
    lazy var statusChoices: [[String:String]] = {
        return [["name":"Worked"], ["name":"Completed"], ["name":"Entered"], ["name":"QA Fail"]]
    }()
    lazy var rowHeight: CGFloat = {
        return 21
    }()
    var timeSlip: TimeSlip? = nil
    var invoice: Invoice? = nil
    
    override func arraySortAscending() -> Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        if timeSlip == nil, let safeInvoice = invoice {
            if let safeTimeSlip = TimeSlip.createTimeSlip(in: managedObjectContext, invoice: safeInvoice) {
                timeSlipObjectController.content = safeTimeSlip
            }
        }
        else {
            if let safeTimeSlip = timeSlip {
                timeSlipObjectController.content = safeTimeSlip
            }
        }
        outerSplitView.autosaveName = "timetracker_slipview_left"
        innerSplitView.autosaveName = "timetracker_slipview_right"
    }
    
    // MARK: - Taget Action

    @IBAction func addWorkSlip(_ sender: Any) {
        if let workSlip = WorkSlip.mr_createEntity(in: managedObjectContext) {
            if let safeTimeSlip = timeSlip {
                safeTimeSlip.addToWorkSlips(workSlip)
            }
        }
    }
    
    @IBAction func deleteWorkSlip(_ sender: Any) {
        let button = sender as? TableViewButton
        if let safeWorkSlip = button?.representedObject as? WorkSlip {
            if let safeTimeSlip = timeSlip {
                safeTimeSlip.removeFromWorkSlips(safeWorkSlip)
                safeWorkSlip.mr_deleteEntity(in: managedObjectContext)
            }
        }
    }
    
    @IBAction func addCodeReview(_ sender: Any) {
        if let codeReview = CodeReview.mr_createEntity(in: managedObjectContext) {
            if let safeTimeSlip = timeSlip {
                safeTimeSlip.addToCodeReviews(codeReview)
            }
        }
    }
    
    @IBAction func deleteCodeReview(_ sender: Any) {
        let button = sender as? TableViewButton
        if let safeCodeReview = button?.representedObject as? CodeReview {
            if let safeTimeSlip = timeSlip {
                safeTimeSlip.removeFromCodeReviews(safeCodeReview)
                safeCodeReview.mr_deleteEntity(in: managedObjectContext)
            }
        }
    }
    
    @IBAction func saveDocument(_ sender: Any) {
        if let safeWindow = self.view.window {
            
            safeWindow.endEditing(for: self)
            let timeSlip = timeSlipObjectController.content as! TimeSlip
            //the object controller binding is not working for the text view, argh!
            //using brute force approach
            timeSlip.notes = notesTextView.string
            if timeSlip.startingDate == nil {
                validationDialog(message: "Please enter the starting date.")
                return
            }
            if timeSlip.duration == 0 {
                dialogSheet(in: safeWindow, message: "No time entered.  Are you sure?", affirmative: "Yes", nonAffirmative: "No") { (response) in
                    if response == .cancel {
                        return
                    }
                    else {
                        safeWindow.sheetParent?.endSheet(safeWindow, returnCode: .OK)
                    }
                }
                return
            }
            safeWindow.sheetParent?.endSheet(safeWindow, returnCode: .OK)
        }
    }
}
