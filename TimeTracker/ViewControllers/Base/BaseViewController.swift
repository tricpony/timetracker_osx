//
//  BaseViewController.swift
//  TimeTracker
//
//  Created by aarthur on 2/3/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

@objc protocol ArrayControllerBindingsSortable:class {
    var arrayControllerSortDescriptorsValue: [NSSortDescriptor] { get set }
    func arrayControllerSortDescriptors() -> [NSSortDescriptor]
}

@objc protocol TreeControllerBindingsSortable:class {
    var treeControllerSortDescriptorsValue: [NSSortDescriptor] { get set }
    func treeControllerSortDescriptors() -> [NSSortDescriptor]
}

class BaseViewController: NSViewController, ArrayControllerBindingsSortable, TreeControllerBindingsSortable {
    lazy var managedObjectContext: NSManagedObjectContext = {
        MagicalRecord.setupCoreDataStack(withStoreNamed:"TimeTracker")
        return NSManagedObjectContext.mr_default()
    }()

    // MARK: - ArrayControllerBindingsSortable

    var arrayControllerSortDescriptorsValue: [NSSortDescriptor] {
        get {
            return arrayControllerSortDescriptors()
        }
        
        //this gets called by the array controller when clicking a column header which apparently triggers a sort
        //without this the app will crash and subsequent attemts to open the offending controller will fail
        //not sure how to fully implement this but for now this will keep things running smoothly
        set(newValue) {

        }
    }

    func arrayControllerSortDescriptors() -> [NSSortDescriptor] {
        //this will do in most cases
        //to customize override in subclass
        return [NSSortDescriptor.init(key: "createDate", ascending: arraySortAscending())]
    }

    func arraySortAscending() -> Bool {
        return false
    }
    
    // MARK: - TreeControllerBindingsSortable

    var treeControllerSortDescriptorsValue: [NSSortDescriptor] {
        get {
            return treeControllerSortDescriptors()
        }
        set (newValue) {
            
        }
    }
    
    func treeControllerSortDescriptors() -> [NSSortDescriptor] {
        //this will do in most cases
        //to customize override in subclass
        return [NSSortDescriptor.init(key: "createDate", ascending: treeSortAscending())]
    }

    func treeSortAscending() -> Bool {
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        if let safeWindowController = windowController() {
            safeWindowController.windowFrameAutosaveName = windowFrameAutoSaveName()
        }
    }

    func windowController() -> NSWindowController? {
        if let safeWindow = self.view.window {
            if let safeWindowController = safeWindow.windowController {
                return safeWindowController
            }
        }
        return nil
    }
    
    func windowFrameAutoSaveName() -> String {
        return "TimeTracker_Main"
    }
    
    func validationDialog(message: String) {
        AlertFactory.validationDialog(message: message)
    }

    //    static func alertSheet(in window: NSWindow, message: String, affirmative: String, nonAffirmative: String, responseHandler: @escaping CompletionResponse) {

    func dialogSheet(in window: NSWindow, message: String, affirmative: String, nonAffirmative: String, responseHandler: @escaping CompletionResponse) {
        AlertFactory.alertSheet(in: window, message: message, affirmative: affirmative, nonAffirmative: nonAffirmative) { (response) in
            if response == NSApplication.ModalResponse.alertFirstButtonReturn {
                responseHandler(.OK)
            }
            else {
                responseHandler(.cancel)
            }

        }
        
    }
    
    @IBAction func cancel(_ sender: Any) {
        if let safeWindow = self.view.window {
            managedObjectContext.rollback()
            safeWindow.sheetParent?.endSheet(safeWindow, returnCode: .OK)
        }
    }

    // MARK: - NSOutlineViewDataSource
    
    func outlineView(_ outlineView: NSOutlineView, persistentObjectForItem item: Any?) -> Any? {
        do {
            if let safeItem = item {
                let nodes = safeItem as? NSTreeNode
                if let safeNode = nodes {
                    let archivable = safeNode.representedObject as? NSCoding
                    let archivedItem = try NSKeyedArchiver.archivedData(withRootObject: archivable as Any, requiringSecureCoding: false)
                    return archivedItem
                }
            }
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        return nil
    }
    
    /**
     This is needed to support outline view state autosave, meaning the outline view remembers which outline disclosures are expanded/collapsed
     This way when the user quits the app, the next time it is launched the outline view will remain in the exact same state as it was last
     To support that itemForPersistentObject wants the treeNode containing the model object, not the model object itself
     func nodeForObject, below, does the heavy lifting for that
     **/
    func outlineView(_ outlineView: NSOutlineView, itemForPersistentObject object: Any) -> Any? {
        guard let treeController = treeController(), let children = treeController.arrangedObjects.children else { return nil }
        return nodeForObject(object, inNodes: children)
    }
    
    func nodeForObject(_ object: Any, inNodes: [NSTreeNode]) -> NSTreeNode? {
        for node in inNodes {
            do {
                if let unArchivedItem = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(object as! Data) as? OutlineViewArchivable{
                    if let nodeObj = node.representedObject as? OutlineViewArchivable {
                        if unArchivedItem.archivableMatchesObject(nodeObj) {
                            return node
                        }
                    }
                }
                if let children = node.children, let item = self.nodeForObject(object, inNodes: children) {
                    return item
                }
                
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        return nil
    }

    func treeController() -> NSTreeController? {
        return nil
    }
}
