//
//  TimeTrackerViewController.swift
//  TimeTracker
//
//  Created by aarthur on 2/2/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

@objc protocol TreeHeaderKeyValueCompliant:class {
    var headerTitle: String { get }
}

@objc protocol TimeTrackerTreeControllerKVC:class {
    var projects: [OutlineViewable] { get }
}

class TimeTrackerViewController: BaseViewController, TimeTrackerTreeControllerKVC, NSMenuItemValidation, NSOutlineViewDataSource, NSControlTextEditingDelegate, TreeHeaderKeyValueCompliant {
    @IBOutlet var projectTreeController: NSTreeController!
    @IBOutlet var timeSlipsArrayController: NSArrayController!
    @IBOutlet weak var tableView: NSTableView!
    @IBOutlet weak var outlineView: NSOutlineView!
    @IBOutlet var projectMenu: NSMenu!
    @IBOutlet var invoiceMenu: NSMenu!
    var didExport = false
    var configurationVersionHasChanged: Bool {
        if let safeVersion = CoreDataUtility.fetchConfigurationVersion(in: managedObjectContext)?.version {
            return safeVersion != ConfigurationUtility.configurationVersion()
        }
        return false
    }

    // MARK: - TreeHeaderKeyValueCompliant

    var headerTitle: String {
        guard let project = selectedProject() else { return "Projects" }
        guard let headerTitle = project.client?.name else { return "Projects" }
        return "Projects of \(headerTitle)"
    }

    // MARK: - TreeControllerBindingsSortable

    override func treeControllerSortDescriptors() -> [NSSortDescriptor] {
        return [NSSortDescriptor.init(key: "startingDate", ascending: treeSortAscending())]
    }

    override func treeSortAscending() -> Bool {
        return true
    }

    // MARK: - TimeTrackerTreeControllerKVC

    var projects: [OutlineViewable] {
        return CoreDataUtility.fetchAllProjects(in: managedObjectContext)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadConfiguration()
        didExport = true
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        outlineView.autosaveTableColumns = true
        outlineView.autosaveName = "com.gigabit.timetracker.outlineview.autosave"
        tableView.autosaveTableColumns = true
    }

    override func viewDidAppear() {
        super.viewDidAppear()
        //Consulting:Rally Remote - BArthur
//        let needsProfileSettings = CoreDataUtility.fetchProjectCount(in: managedObjectContext, named: "Consulting:Rally Remote - BArthur") == 0
        let needsProfileSettings = CoreDataUtility.fetchEnterpriseCount(in: managedObjectContext) == 0

        if needsProfileSettings {
            presentImportTemplateSheet()
        }
    }
    
    // MARK: - Taget Action

    @IBAction func saveDocument(_ sender: Any) {
        if let safeWindow = self.view.window {
            safeWindow.endEditing(for: self)
            managedObjectContext.mr_saveToPersistentStoreAndWait()
        }
    }
    
    @IBAction func addTimeSlip(_ sender: Any) {
        if selectedProjectTreeObjectIsInvoice() {
            if let invoice = projectTreeController.selectedObjects.first as? Invoice {
                if let safeTimeSlip = TimeSlip.createTimeSlip(in: managedObjectContext, invoice: invoice) {
                    presentTimeSlipSheet(invoice: invoice, timeSlip: safeTimeSlip)
                }
            }
        }
        else {
            validationDialog(message: "For which month?")
        }
    }
    
    @IBAction func editTimeSlip(_ sender: Any) {
        if let safeTimeSlip = timeSlipsArrayController.selectedObjects.first as? TimeSlip {
            if let safeInvoice = safeTimeSlip.invoice {
                presentTimeSlipSheet(invoice: safeInvoice, timeSlip: safeTimeSlip);
            }
        }
    }

    @IBAction func expandAddProjectMenu(_ sender: Any) {
        if let safeEvent = NSApplication.shared.currentEvent {
            if let button = sender as? NSButton {
                if let safeMenu = button.menu {
                    NSMenu.popUpContextMenu(safeMenu, with: safeEvent, for: button)
                }
            }
        }
    }
    
    @IBAction func addClient(_ sender: Any) {
        presentClientSheet()
    }
    
    func selectedMenuItemTitle(menuItem: Any) -> String {
        if (menuItem as? NSObject)!.className() == "NSMenuItem" {
            guard let menuItem = menuItem as? NSMenuItem else { return "" }
            return menuItem.title
        }
        return ""
    }
    
    @IBAction func exportInvoice(_ sender: Any) {
        guard selectedProjectTreeObjectIsInvoice() else { validationDialog(message: "Which month?"); return }
        
        let invoice = treeController()?.selectedObjects.first as? Invoice
        switch selectedMenuItemTitle(menuItem: sender) {
        case "Export Selected", "Preview Selected":
            guard let selectedSlips = timeSlipsArrayController.selectedObjects as? [TimeSlip] else { return }
            guard let clonedInvoice = invoice?.clone(forSelectedSlips: selectedSlips, ctx: managedObjectContext) else { return }
            
            let buffer = clonedInvoice.exportContent()
            guard let clonedClient = clonedInvoice.project?.client else { return }
            let clonedEnterprise = clonedClient.enterprise

            managedObjectContext.mr_deleteObjects([clonedEnterprise] as NSFastEnumeration)
            managedObjectContext.mr_saveToPersistentStoreAndWait()

            if selectedMenuItemTitle(menuItem: sender) == "Preview Selected" {
                performSegue(withIdentifier: "PreviewSeque", sender: buffer)
            }else{
                openSavePanel(buffer: buffer)
            }
        case "Preview All":
            guard let buffer = invoice?.exportContent() else { return }
            managedObjectContext.mr_saveToPersistentStoreAndWait()
            performSegue(withIdentifier: "PreviewSeque", sender: buffer)
        default:
            guard let buffer = invoice?.exportContent() else { return }
            managedObjectContext.mr_saveToPersistentStoreAndWait()
            openSavePanel(buffer: buffer)
        }
    }
    
    @IBAction func addProject(_ sender: Any) {
        presentAddProjectSheet()
    }
    
    @IBAction func editProject(_ sender: Any) {
        let selectedOutlineViewItem = projectTreeController.selectedObjects.first
        if (selectedOutlineViewItem is Project) {
            presentEditProjectSheet(project: selectedOutlineViewItem as? Project)
        }
    }
    
    @IBAction func doubleTapOutlineView(_ sender: Any) {
        if selectedProjectTreeObjectIsInvoice() {
            expandInvoiceMenu(sender)
        }
        else {
            editProject(sender)
        }
    }
    
    @IBAction func addInvoice(_ sender: Any) {
        let project = selectedProject()
        
        if let safeInvoice = Invoice.createInvoice(in: managedObjectContext, project: project!) {
            if let _ = TimeSlip.createTimeSlip(in: managedObjectContext, invoice: safeInvoice) {
                self.willChangeValue(forKey: "projects")
                managedObjectContext.mr_saveToPersistentStoreAndWait()
                self.didChangeValue(forKey: "projects")
            }
        }
    }
    
    @IBAction func deleteTimeTrackerObject(_ sender: Any) {
        let selectedObjects = projectTreeController.selectedObjects
        if selectedObjects.count == 0 {
            return
        }
        self.willChangeValue(forKey: "projects")
        if selectedProjectTreeObjectIsInvoice() {
            if let invoice = selectedObjects.first as? Invoice {
                for nextInvoice in selectedObjects as! [Invoice] {
                    nextInvoice.project?.removeFromInvoices(invoice)
                    nextInvoice.mr_deleteEntity(in: managedObjectContext)
                }
            }
        }
        else {
            for nextProject in selectedObjects as! [Project] {
                nextProject.mr_deleteEntity(in: managedObjectContext)
            }
        }
        managedObjectContext.mr_saveToPersistentStoreAndWait()
        self.didChangeValue(forKey: "projects")
    }
    
    @IBAction func deleteTimeSlips(_ sender: Any) {
        let selectedObjects = timeSlipsArrayController.selectedObjects
        if selectedObjects?.count == 0 {
            return
        }
        for nextItem in selectedObjects as! [TimeSlip] {
            nextItem.mr_deleteEntity(in: managedObjectContext)
            managedObjectContext.mr_saveToPersistentStoreAndWait()
        }
        projectTreeController.rearrangeObjects()
    }

    @IBAction func importOldTimeSlips(_ sender: Any) {
        guard let window = view.window else { return }

        dialogSheet(in: window, message: "Are you sure?", affirmative: "Yes", nonAffirmative: "No") { [weak self] (response) in
            guard let strongSelf = self else { return }
            if response == .OK {
                print("tapped yes")
                strongSelf.importOldTime()
            }
            else {
                print("tapped no")
            }
        }
    }

    // MARK: - Convenience

    func openSavePanel(buffer: String) {
        guard let window = view.window else { return }
        
        PanelFactory.openSavePanel(for: window) { [weak self] (response, url) in
            guard let strongSelf = self else { return }
            if response == .OK {
                do {
                    try buffer.write(to: url!, atomically: true, encoding: .utf8)
                    guard let invoice = strongSelf.treeController()?.selectedObjects.first as? Invoice else { return }
                    invoice.billed = true
                    strongSelf.managedObjectContext.mr_saveToPersistentStoreAndWait()

                }
                catch {
                    print("Failed To Save Export")
                }
            }
        }
    }
    
    func importTemplate() {
        guard let window = view.window else { return }
        PanelFactory.openFindPanel(for: window) { [weak self](response, url) in
            guard let strongSelf = self else { return }
            if response == .OK {
                strongSelf.parseImportFile(at: url!)
            }
        }
    }
    
    func importOldTime() {
        guard let fromProject = CoreDataUtility.fetchProject(in: managedObjectContext, named: "Consulting:Rally Remote - BArthur - orig") else { return }
        guard let toProject = CoreDataUtility.fetchProject(in: managedObjectContext, named: "Consulting:Rally Remote - BArthur") else { return }
        let fromTimeSlips = fromProject.flattenedTimeSlips()
        toProject.statementOfwork = fromProject.statementOfwork
        toProject.moveTimeSlipsHere(timeSlips: fromTimeSlips)
        managedObjectContext.delete(fromProject)
        managedObjectContext.mr_saveToPersistentStoreAndWait()
    }
    
    func parseImportFile(at: URL) {
        self.willChangeValue(forKey: "projects")
        self.willChangeValue(forKey: "headerTitle")
        ImportWorker.importUserTemplate(isLiveData: false, url: at, ctx: managedObjectContext) { [weak self](error) in
            guard let strongSelf = self else { return }

            if let safeError = error {
                let message = "Import failed: \(safeError.localizedDescription)"
                strongSelf.validationDialog(message: message)
            }
            else {
                guard let window = strongSelf.view.window else { return }

                strongSelf.dialogSheet(in: window, message: "Import Old Time?", affirmative: "Yes", nonAffirmative: "No", responseHandler: { (response) in
                    if response == .OK {
                        print("Agreed")
                        strongSelf.importOldTime()
                    }
                    else {
                        print("Not Agreed")
                    }
                })
            }
        }
        self.didChangeValue(forKey: "projects")
        self.didChangeValue(forKey: "headerTitle")
    }

    func presentImportTemplateSheet() {
        let storyboard = NSStoryboard(name: "ImportTemplateViewController", bundle: nil)
        let windowController = storyboard.instantiateInitialController() as! NSWindowController
        
        if let window = self.view.window {
            if let sheet = windowController.window {
                window.beginSheet(sheet, completionHandler: { [weak self](response: NSApplication.ModalResponse) in
                    guard let strongSelf = self else { return }
                    if response == .OK {
                        strongSelf.importTemplate()
                    }
                    else {
                        strongSelf.presentEnterpriseSheet()
                    }
                })
            }
        }
    }
    
    func presentEnterpriseSheet() {
        let storyboard = NSStoryboard(name: "EnterpriseViewController", bundle: nil)
        let windowController = storyboard.instantiateInitialController() as! NSWindowController
        
        if let window = self.view.window {
            if let sheet = windowController.window {
                window.beginSheet(sheet, completionHandler: { [weak self](response: NSApplication.ModalResponse) in
                    guard let strongSelf = self else { return }
                    strongSelf.managedObjectContext.mr_saveToPersistentStoreAndWait()
                    strongSelf.presentClientSheet()
                })
            }
        }
    }
    
    func presentClientSheet() {
        let storyboard = NSStoryboard(name: "ClientViewController", bundle: nil)
        let windowController = storyboard.instantiateInitialController() as! NSWindowController
        
        if let window = self.view.window {
            if let sheet = windowController.window {
                window.beginSheet(sheet, completionHandler: { [weak self](response: NSApplication.ModalResponse) in
                    guard let strongSelf = self else { return }
                    strongSelf.managedObjectContext.mr_saveToPersistentStoreAndWait()
                })
            }
        }
    }
    
    func presentAddProjectSheet(project: Project? = nil) {
        let storyboard = NSStoryboard(name: "ProjectViewController", bundle: nil)
        let windowController = storyboard.instantiateInitialController() as! NSWindowController
        
        if let window = self.view.window {
            if let sheet = windowController.window {
                window.beginSheet(sheet, completionHandler: { [weak self](response: NSApplication.ModalResponse) in
                    guard let strongSelf = self else { return }
                    strongSelf.willChangeValue(forKey: "projects")
                    strongSelf.managedObjectContext.mr_saveToPersistentStoreAndWait()
                    strongSelf.didChangeValue(forKey: "projects")
                })
            }
        }
    }
    
    func presentEditProjectSheet(project: Project? = nil) {
        let storyboard = NSStoryboard(name: "EditProjectViewController", bundle: nil)
        let windowController = storyboard.instantiateInitialController() as! NSWindowController
        
        if let window = self.view.window {
            if let sheet = windowController.window {
                if let vc = windowController.contentViewController as? EditProjectViewController {
                    vc.project = project
                    window.beginSheet(sheet, completionHandler: { [weak self](response: NSApplication.ModalResponse) in
                        guard let strongSelf = self else { return }
                        strongSelf.willChangeValue(forKey: "projects")
                        strongSelf.managedObjectContext.mr_saveToPersistentStoreAndWait()
                        strongSelf.didChangeValue(forKey: "projects")
                    })
                }
            }
        }
    }
    
    func presentTimeSlipSheet(invoice: Invoice, timeSlip: TimeSlip? = nil) {
        let storyboard = NSStoryboard(name: "TimeSlipViewController", bundle: nil)
        let windowController = storyboard.instantiateInitialController() as! NSWindowController
        
        if let window = self.view.window {
            if let sheet = windowController.window {
                if let vc = windowController.contentViewController as? TimeSlipViewController {
                    vc.invoice = invoice
                    vc.timeSlip = timeSlip
                }
                window.beginSheet(sheet, completionHandler: { [weak self](response: NSApplication.ModalResponse) in
                    guard let strongSelf = self else { return }
                    strongSelf.willChangeValue(forKey: "projects")
                    strongSelf.managedObjectContext.mr_saveToPersistentStoreAndWait()
                    strongSelf.didChangeValue(forKey: "projects")
                })
            }
        }
    }

    func expandInvoiceMenu(_ sender: Any) {
        if let safeEvent = NSApplication.shared.currentEvent {
            if let safeMenu = outlineView.menu {
                NSMenu.popUpContextMenu(safeMenu, with: safeEvent, for: sender as! NSView)
            }
        }
    }

    func selectedProjectTreeObjectIsInvoice() -> Bool {
        let invoice = projectTreeController.selectedObjects.first
        return invoice is Invoice
    }
    
    func selectedProject() -> Project? {
        if selectedProjectTreeObjectIsInvoice() {
            if let invoice = projectTreeController.selectedObjects.first as? Invoice {
                return invoice.project
            }
        }
        if let project = projectTreeController.selectedObjects.first {
            return project as? Project
        }
        return nil
    }
        
    // MARK: - NSControlTextEditingDelegate

    func control(_ control: NSControl, textView: NSTextView, doCommandBy commandSelector: Selector) -> Bool {
        if textView.method(for: commandSelector) == textView.method(for: #selector(moveDown)) {
            let item = treeController()?.selectedNodes.first
            outlineView.expandItem(item)
            return true
        }
        return false
    }

    // MARK: - NSOutlineViewDataSource
    
    override func treeController() -> NSTreeController? {
        return projectTreeController
    }

    override func outlineView(_ outlineView: NSOutlineView, persistentObjectForItem item: Any?) -> Any? {
        return super.outlineView(outlineView, persistentObjectForItem: item)
    }

    override func outlineView(_ outlineView: NSOutlineView, itemForPersistentObject object: Any) -> Any? {
        return super.outlineView(outlineView, itemForPersistentObject: object)
    }

    // MARK: - NSMenuItemValidation
    
    func validateMenuItem(_ menuItem: NSMenuItem) -> Bool {
        let menuTitle = menuItem.title
        if menuTitle == "Add Project" {
            
            //add project menu item should always be enabled
            return true
        }
        return CoreDataUtility.fetchProjectCount(in: managedObjectContext) > 0
    }
    
    // MARK: - Configuration
    
    func needsConfigurationLoad() -> Bool {
        if didExport {
            return false
        }
        if CoreDataUtility.fetchConfigurationVersionCount(in: managedObjectContext) == 0 {
            return true
        }
        
        return configurationVersionHasChanged
    }
    
    func loadConfiguration() {
        guard needsConfigurationLoad() else { return }
        
        clearSettings()
        let settings = ConfigurationUtility.configurationSettings()
        if let version = ConfigVersion.mr_createEntity(in: managedObjectContext) {
            for nextSetting in settings {
                let payload = ConfigurationUtility.asJSON(nextSetting)
                if let freshConfig = JsonUtility<Configuration>.parseJSON(payload, ctx: managedObjectContext) {
                    version.addToConfigurations(freshConfig)
                    if let relConfigs = nextSetting["imt_relationships"] as? [[String:Any]] {
                        for nextRelConfig in relConfigs {
                            let r_payload = ConfigurationUtility.asJSON(nextRelConfig)
                            if let freshRelConfig = JsonUtility<ConfigRelationship>.parseJSON(r_payload, ctx: managedObjectContext) {
                                freshConfig.addToRelationships(freshRelConfig)
                            }
                        }
                    }
                }
            }
            version.version = ConfigurationUtility.configurationVersion()
            version.header = ConfigurationUtility.configurationHeader()
        }
        managedObjectContext.mr_saveToPersistentStoreAndWait()
    }
    
    func clearSettings() {
        if let safeVersion = CoreDataUtility.fetchConfigurationVersion(in: managedObjectContext) {
            safeVersion.mr_deleteEntity(in: managedObjectContext)
            managedObjectContext.mr_saveToPersistentStoreAndWait()
        }
    }

    // MARK: - Storyboard
    
    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        if segue.identifier == "PreviewSeque" {
            let windowController = segue.destinationController as! NSWindowController
            if let buffer = sender as? String {
                if let vc = windowController.contentViewController as? ExportPreviewViewController {
                    vc.previewBuffer = buffer;
                    vc.timeTrackerVC = self
                }
            }
        }
    }

}
