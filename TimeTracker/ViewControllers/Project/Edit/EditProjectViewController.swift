//
//  EditProjectViewController.swift
//  TimeTracker
//
//  Created by aarthur on 2/17/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

class EditProjectViewController: BaseViewController, NSTableViewDelegate {
    @IBOutlet var projectObjectController: NSObjectController!
    @IBOutlet var statementOfWorkArrayController: NSArrayController!
    @IBOutlet weak var tableView: NSTableView!
    var project: Project!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        projectObjectController.content = project
        if let safeProject = project {
            if statementOfWorkArrayController.setSelectedObjects([safeProject.statementOfwork as Any]) == false {
                print("Failed!!")
            }
        }
        tableView.delegate = self
    }

    override func windowFrameAutoSaveName() -> String {
        return "TimeTracker_Project"
    }

    @IBAction func editStatementOfWork(_ sender: Any) {
        presentClientSheet()
    }
    
    func presentClientSheet() {
        let storyboard = NSStoryboard(name: "ClientViewController", bundle: nil)
        let windowController = storyboard.instantiateInitialController() as! NSWindowController
        
        if let window = self.view.window {
            if let sheet = windowController.window {
                guard let selectedTableViewObject = statementOfWorkArrayController.selectedObjects.first else { return }
                let sow = selectedTableViewObject as? StatementOfWork
                if let client = sow?.client {
                    if let vc = windowController.contentViewController as? ClientViewController {
                        vc.client = client
                        window.beginSheet(sheet, completionHandler: { [weak self](response: NSApplication.ModalResponse) in
                            guard let strongSelf = self else { return }
                            strongSelf.managedObjectContext.mr_saveToPersistentStoreAndWait()
                        })
                    }
                }
            }
        }
    }

    @IBAction func saveDocument(_ sender: Any) {
        if let safeWindow = self.view.window {
            safeWindow.endEditing(for: self)
            safeWindow.sheetParent?.endSheet(safeWindow, returnCode: .OK)
        }
    }
    
    // MARK: - NSTableViewDelegate

    /**
     Tried using bindings to do this but it never worked, argh!!
    **/
    func tableViewSelectionDidChange(_ notification: Notification) {
        if let selectedSOW = statementOfWorkArrayController.selectedObjects.first as? StatementOfWork {
            if let safeProject = project {
                safeProject.statementOfwork = selectedSOW
            }
        }
    }

}
