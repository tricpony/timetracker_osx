//
//  ProjectViewController.swift
//  TimeTracker
//
//  Created by aarthur on 2/9/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

@objc protocol ClientsTreeControllerKVC:class {
    var clients: [OutlineViewable] { get }
}

class ProjectViewController: BaseViewController, ClientsTreeControllerKVC, NSOutlineViewDataSource {
    @IBOutlet var projectObjectController: NSObjectController!
    @IBOutlet var clientTreeController: NSTreeController!
    @IBOutlet weak var outlineView: NSOutlineView!
    var project: Project? = nil
    
    // MARK: - ClientsTreeControllerKVC
    
    var clients: [OutlineViewable] {
        return CoreDataUtility.fetchAllClients(in: managedObjectContext)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        outlineView.autosaveTableColumns = true
        fillProject()
    }
    
    func fillProject() {
        if let project = self.project ?? Project.mr_createEntity(in: managedObjectContext) {
            
            //if there are no invoices then this is a new project so add one
            if let count = project.invoices?.count,
                count == 0,
                let invoice = Invoice.mr_createEntity(in: managedObjectContext) {
                project.addToInvoices(invoice)
                TimeSlip.createTimeSlip(in: managedObjectContext, invoice: invoice)
            }
            projectObjectController.addObject(project)
            self.project = project
        }
    }
        
    @IBAction func saveDocument(_ sender: Any) {
        if let safeWindow = self.view.window {
            safeWindow.endEditing(for: self)
            let selectedObjects = clientTreeController.selectedObjects
            let isSOW = selectedObjects.first is StatementOfWork
            
            if let safeProject = self.project, safeProject.hasStatmentOfWork() == false {
                guard isSOW else {
                validationDialog(message: "Please choose a statement of work.")
                return }
                
                if safeProject.hasStatmentOfWork() == false {
                    let sow = selectedObjects.last as! StatementOfWork
                    let project = projectObjectController.content as! Project
                    
                    project.statementOfwork = sow
                    project.client = sow.client
                    project.registerProjectRelationships()
                }
            }
            
            safeWindow.sheetParent?.endSheet(safeWindow, returnCode: .OK)
        }
    }
    
    @IBAction func editClient(_ sender: Any) {
        guard let selectedOutlineViewItem = clientTreeController.selectedObjects.first else { return }
        if (selectedOutlineViewItem is Client) {
            presentClientSheet()
        }
    }

    func presentClientSheet() {
        let storyboard = NSStoryboard(name: "ClientViewController", bundle: nil)
        let windowController = storyboard.instantiateInitialController() as! NSWindowController
        
        if let window = self.view.window {
            if let sheet = windowController.window {
                let selectedObject = clientTreeController.selectedObjects.first
                if selectedObject is Client {
                    if let vc = windowController.contentViewController as? ClientViewController {
                        vc.client = selectedObject as? Client
                        window.beginSheet(sheet, completionHandler: { [weak self](response: NSApplication.ModalResponse) in
                            guard let strongSelf = self else { return }
                            strongSelf.willChangeValue(forKey: "client.statementsOfWork")
                            strongSelf.managedObjectContext.mr_saveToPersistentStoreAndWait()
                            strongSelf.didChangeValue(forKey: "client.statementsOfWork")
                            strongSelf.clientTreeController.rearrangeObjects()
                        })
                    }
                }
            }
        }
    }

    // MARK: - NSOutlineViewDataSource
    
    override func treeController() -> NSTreeController? {
        return clientTreeController
    }
    
    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        return true
    }

    override func outlineView(_ outlineView: NSOutlineView, persistentObjectForItem item: Any?) -> Any? {
        return super.outlineView(outlineView, persistentObjectForItem: item)
    }
    
    override func outlineView(_ outlineView: NSOutlineView, itemForPersistentObject object: Any) -> Any? {
        return super.outlineView(outlineView, itemForPersistentObject: object)
    }

}
