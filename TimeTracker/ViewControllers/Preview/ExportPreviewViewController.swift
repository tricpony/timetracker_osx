//
//  ExportPreviewViewController.swift
//  TimeTracker
//
//  Created by aarthur on 2/20/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

@objc protocol JumpChoicesBindingKeyValueCompliance:class {
    var jumpChoices: [[String:String]] { get }
}

class ExportPreviewViewController: BaseViewController, JumpChoicesBindingKeyValueCompliance {
    @IBOutlet var textView: NSTextView!
    var previewBuffer: String = ""
    weak var timeTrackerVC: TimeTrackerViewController? = nil
    lazy var jumpChoices: [[String:String]] = {
        return [["name":"IDENTITY"], ["name":"SERVICE"], ["name":"CUSTOMER"], ["name":"JOB"], ["name":"TIMEPERIOD"]]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        textView.string = previewBuffer
    }

    @IBAction func exportAction(_ sender: Any) {
        if let window = view.window, let vc = timeTrackerVC {
            vc.openSavePanel(buffer: previewBuffer)
            window.close()
        }
    }
    
    @IBAction func jumpToSection(_ sender: Any) {
        let popup = sender as? NSPopUpButton
        
        guard var title = popup?.selectedItem?.title else { return }
        title = "<object type=\"\(title)"
        
        if let cr = textView.string.range(of: title) {
            if cr.location != NSNotFound {
                textView.scrollRangeToVisible(cr)
                textView.setSelectedRange(cr)
            }
        }
    }
}
