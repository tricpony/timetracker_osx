//
//  ClientViewController.swift
//  TimeTracker
//
//  Created by aarthur on 2/9/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

class ClientViewController: BaseViewController {
    @IBOutlet var sowArrayController: NSArrayController!
    @IBOutlet var clientObjectController: NSObjectController!
    @IBOutlet weak var tableView: NSTableView!
    var statementsOfWork = [StatementOfWork]()
    var client: Client? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        tableView.autosaveTableColumns = true
        fillClient()
    }
    
    func fillClient() {
        if let safeClient = client {
            clientObjectController.content = safeClient
        }
        else {
            fillNewCient()
        }
    }

    func fillNewCient() {
        if let safeEnterprise = Enterprise.enterpriseObject(in: managedObjectContext) {
            if safeEnterprise.clients?.count == 0 {
                if let client = Client.createClient(in: managedObjectContext, enterprise: safeEnterprise, name: "New Client") {
                    clientObjectController.addObject(client)
                }
            }
            else {
                if let client = Client.createClient(in: managedObjectContext, enterprise: safeEnterprise, name: "New Client") {
                    clientObjectController.addObject(client)
                }
            }
        }
    }
    
    override func windowFrameAutoSaveName() -> String {
        return "TimeTracker_Client"
    }

    // MARK: - Target Action

    @IBAction func saveDocument(_ sender: Any) {
        if let safeWindow = self.view.window {
            
            safeWindow.endEditing(for: self)
            let client = clientObjectController.content as! Client
            let clientNameLength = client.name?.count ?? 0
            
            guard clientNameLength > 0 else {
                validationDialog(message: "Please enter client name.")
                return
            }
            guard client.allSOW_validForSave() else {
                validationDialog(message: "Missing statement of work name or date.")
                return
            }
            safeWindow.sheetParent?.endSheet(safeWindow, returnCode: .OK)
        }
    }
    
    @IBAction func addSOW(_ sender: Any) {
        let client = clientObjectController.content as! Client
        if let safeSOW = StatementOfWork.createSOW(in: managedObjectContext, forClient: client) {
            sowArrayController.addObject(safeSOW)
            client.addToStatementsOfWork(safeSOW)
        }
    }
    
    
    @IBAction func delete(_ sender: Any) {
        let client = clientObjectController.content as! Client
        let selectedSOW = sowArrayController.selectedObjects.first as! StatementOfWork
        
        client.removeFromStatementsOfWork(selectedSOW)
        selectedSOW.mr_deleteEntity(in: managedObjectContext)
    }
}
