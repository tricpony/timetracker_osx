//
//  ImportTemplateViewController.swift
//  TimeTracker
//
//  Created by aarthur on 2/25/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

class ImportTemplateViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func windowFrameAutoSaveName() -> String {
        return "TimeTracker_Import"
    }

    // MARK: - Taget Action

    @IBAction func importTemplate(_ sender: Any) {
        guard let window = view.window else { return }
        window.sheetParent?.endSheet(window, returnCode: .OK)
    }

    @IBAction func manualEntry(_ sender: Any) {
        guard let window = view.window else { return }
        window.sheetParent?.endSheet(window, returnCode: .cancel)
    }
}
