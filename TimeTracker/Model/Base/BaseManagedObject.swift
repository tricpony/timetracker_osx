//
//  BaseManagedObject.swift
//  TimeTracker
//
//  Created by aarthur on 2/14/19.
//  Copyright © 2019 Gigabit LLC. All rights reserved.
//

import Cocoa

@objc protocol TimeSlipsKeyValueCompliant:class {
    var flattenedTimeSlipsValue: [TimeSlip] { get }
    func flattenedTimeSlips() -> [TimeSlip]
}

@objc protocol OutlineViewable:class {
    var isLeaf:Bool { get }
    var outlineCount:Int { get }
    var outlineChildren:[OutlineViewable] { get }
}

protocol OutlineViewArchivable {
    var archivableDate: Date { get }
    func archivableMatchesObject(_ value: OutlineViewArchivable) -> Bool
}

@objc protocol Importable:class {
    var importableName: String? { set get }
    func importValue(_ value: [String : String])
    func assemble(_ objects: [String : Any], isLive: Bool)
}

@objc protocol ConfigurationKeyValueCompliant:class {
    var createNextImtIdentifier: String { get }
    var clientJobIds: String { get }
}

@objc(BaseManagedObject)
public class BaseManagedObject: NSManagedObject, TimeSlipsKeyValueCompliant, ConfigurationKeyValueCompliant {
    
    @NSManaged public var createDate: NSDate?

    func createDateTimeInterval() -> Double {
        return createDate?.timeIntervalSinceNow ?? 0
    }
    
    // MARK: - Convenience
    
    /**
     Copy attribute values from self to me
     
     - Parameters me: The object being copied to.
    */
    func copyAttributes(to me: BaseManagedObject) {
        let atts = entity.attributesByName
        let attKeys = atts.keys
        
        for nextKey in attKeys {
            me.setValue(value(forKey: nextKey), forKey: nextKey)
        }
    }
    
    // MARK: - ConfigurationKeyValueCompliant

    var createNextImtIdentifier: String {
        let ctx = NSManagedObjectContext.mr_default()
        let className = self.className
        guard let config = CoreDataUtility.fetchConfiguration(in: ctx, matcingClassName: className) else {
            fatalError()
        }
        let nextID = config.nextID()
        config.nextIMT_ID = nextID
        ctx.mr_saveToPersistentStoreAndWait()
        return "z\(nextID)"
    }
    
    var clientJobIds: String {
        return ""
    }

    // MARK: - TimeSlipsKeyValueCompliant

    var flattenedTimeSlipsValue: [TimeSlip] {
        return flattenedTimeSlips()
    }

    @objc func flattenedTimeSlips() -> [TimeSlip] {
        return [TimeSlip]()
    }

    func exportConfig(_ config: Configuration, invoice: Invoice) -> String {
        return ""
    }

    public override func awakeFromInsert() {
        super.awakeFromInsert()
        setValue(NSDate(), forKey:"createDate")
    }
}

extension BaseManagedObject: Importable {

    @objc var importableName: String? {
        get {
            return ""
        }
        
        set (newValue) {
            return
        }
    }
    
    @objc func importValue(_ value: [String : String]) {
        guard let key = value.keys.first else { return }
        let object = value[key]
        
        self.setValue(object, forKey: key)
        return
    }
    
    @objc func assemble(_ objects: [String : Any], isLive: Bool) {
        return
    }
}

