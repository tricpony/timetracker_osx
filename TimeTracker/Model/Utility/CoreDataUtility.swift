//
//  CoreDataUtility.swift
//  MovieLab
//
//  Created by aarthur on 5/10/17.
//  Copyright © 2017 Gigabit LLC. All rights reserved.
//

import Foundation

class CoreDataUtility {
    
    // MARK: - Fetch Count

    class func fetchConfigurationVersionCount(in ctx: NSManagedObjectContext) -> UInt {
        return ConfigVersion.mr_countOfEntities(with: nil, in: ctx)
    }
    
    class func fetchEnterpriseCount(in ctx: NSManagedObjectContext) -> UInt {
        return Enterprise.mr_countOfEntities(with: nil, in: ctx)
    }
    
    class func fetchProjectCount(in ctx: NSManagedObjectContext) -> UInt {
        return Project.mr_countOfEntities(with: nil, in: ctx)
    }
    
    class func fetchProjectCount(in ctx: NSManagedObjectContext, named: String) -> UInt {
        let q  = equalPredicate(key: "name", value: named)
        return Project.mr_countOfEntities(with: q, in: ctx)
    }
    
    class func fetchClientCount(in ctx: NSManagedObjectContext, named: String) -> UInt {
        let q  = equalPredicate(key: "name", value: named)
        return Client.mr_countOfEntities(with: q, in: ctx)
    }
    
    // MARK: - Fetch Objects

    class func fetchConfigurationVersion(in ctx: NSManagedObjectContext) -> ConfigVersion? {
        let me = ConfigVersion.mr_findAll(in: ctx)?.last as? ConfigVersion
        return me
    }
    
    class func fetchEnterprise(in ctx: NSManagedObjectContext) -> Enterprise? {
        let me = Enterprise.mr_findAll(in: ctx)?.last as? Enterprise
        return me
    }
    
    class func fetchAllClients(in ctx: NSManagedObjectContext) -> [Client] {
        let clients = Client.mr_findAll(in: ctx)
        return clients as! [Client]
    }
    
    class func fetchAllConfigurations(in ctx: NSManagedObjectContext) -> [Configuration] {
        let configurations = Configuration.mr_findAllSorted(by: "order", ascending: true)
        return configurations as! [Configuration]
    }
    
    class func fetchAllProjects(in ctx: NSManagedObjectContext) -> [Project] {
        let projects = Project.mr_findAll(in: ctx)
        return projects as! [Project]
    }
    
    class func fetchProject(in ctx: NSManagedObjectContext, named: String) -> Project? {
        let q = equalPredicate(key: "name", value: named)
        return Project.mr_findFirst(with: q, in: ctx) ?? nil
    }
    
    class func fetchProject(in ctx: NSManagedObjectContext, notNamed: String) -> Project? {
        let q = notEqualToAnyPredicate(key: "name", value: notNamed)
        
        return Project.mr_findFirst(with: q, in: ctx) ?? nil
    }
    
    class func fetchAnyTimeslip(in ctx: NSManagedObjectContext, betweenDates: [Date], anti notOnInvoice: Invoice) -> TimeSlip? {
        guard let firstDate = betweenDates.first else { return nil }
        guard let lastDate = betweenDates.last else { return nil }
        var q = greaterThanOrEqualToPredicate(key: "startingDate", value: firstDate)
        let rq = lessThanOrEqualToPredicate(key: "startingDate", value: lastDate)
        let lq = notEqualToPredicate(key: "invoice", value: notOnInvoice)
        
        q = NSCompoundPredicate.init(andPredicateWithSubpredicates: [q,rq,lq])
        return TimeSlip.mr_findFirst(with: q, in: ctx)
    }
    
    class func fetchAllTimeSlips(in ctx: NSManagedObjectContext) -> [Project] {
        let projects = TimeSlip.mr_findAll(in: ctx)
        return projects as! [Project]
    }
    
    class func fetchTimeSlipsForProject(in ctx: NSManagedObjectContext, project: Project) -> [TimeSlip] {
        let q = inSetForKey("invoice", values: (project.invoices as? Set)!)
        return TimeSlip.mr_findAll(with: q, in: ctx) as! [TimeSlip]
    }

    class func fetchTimeSlip(matching id: String, invoice: Invoice, in ctx: NSManagedObjectContext) -> [TimeSlip] {
        var q = equalPredicate(key: "invoice", value: invoice)
        let rq = equalPredicate(key: "imtIdentifier", value: id)
        var result = [TimeSlip]()
        q = NSCompoundPredicate.init(andPredicateWithSubpredicates: [q,rq])
        result = (TimeSlip.mr_findAll(with: q, in: ctx) as? [TimeSlip])!
        
        return result
    }
    
    class func fetchTimeSlip(invoice: Invoice, in ctx: NSManagedObjectContext) -> [TimeSlip] {
        let q = equalPredicate(key: "invoice", value: invoice)
        var result = [TimeSlip]()
        result = (TimeSlip.mr_findAll(with: q, in: ctx) as? [TimeSlip])!
        
        return result
    }
    
    class func fetchConfiguration(in ctx: NSManagedObjectContext, matcingClassName: String) -> Configuration? {
        let q = equalPredicate(key: "identifier", value: matcingClassName)
        return Configuration.mr_findFirst(with: q, in: ctx)
    }
    
    class func fetchConfiguration(in ctx: NSManagedObjectContext, matchingObjectType: String) -> Configuration? {
        let q = equalPredicate(key: "objectType", value: matchingObjectType)
        return Configuration.mr_findFirst(with: q, in: ctx)
    }
    
    class func fetchConfigRelationship(in ctx: NSManagedObjectContext, configCless: String, templateSubstitution: String) -> ConfigRelationship? {
        var q = equalPredicate(key: "configuration.identifier", value: configCless)
        let rq = equalPredicate(key: "templateSubstitution", value: templateSubstitution)
        
        q = NSCompoundPredicate.init(andPredicateWithSubpredicates: [q,rq])
        return ConfigRelationship.mr_findFirst(with: q, in: ctx) ?? nil
    }
    
    // MARK: - Pre-fabbed predicates
    
    class func equalPredicate(key: String, value: Any) -> NSPredicate {
        let lhs: NSExpression = NSExpression.init(forKeyPath: key)
        let rhs: NSExpression = NSExpression.init(forConstantValue: value)
        let q: NSPredicate = NSComparisonPredicate.init(leftExpression: lhs,
                                                        rightExpression: rhs,
                                                        modifier: NSComparisonPredicate.Modifier.direct,
                                                        type: NSComparisonPredicate.Operator.equalTo,
                                                        options: NSComparisonPredicate.Options.diacriticInsensitive)
        
        return q
    }
    
    class func equalToAnyPredicate(key: String, value: Any) -> NSPredicate {
        let lhs: NSExpression = NSExpression.init(forKeyPath: key)
        let rhs: NSExpression = NSExpression.init(forConstantValue: value)
        let q: NSPredicate = NSComparisonPredicate.init(leftExpression: lhs,
                                                        rightExpression: rhs,
                                                        modifier: NSComparisonPredicate.Modifier.any,
                                                        type: NSComparisonPredicate.Operator.equalTo,
                                                        options: NSComparisonPredicate.Options.diacriticInsensitive)
        
        return q
    }
    
    class func notEqualToAnyPredicate(key: String, value: Any) -> NSPredicate {
        let lhs: NSExpression = NSExpression.init(forKeyPath: key)
        let rhs: NSExpression = NSExpression.init(forConstantValue: value)
        let q: NSPredicate = NSComparisonPredicate.init(leftExpression: lhs,
                                                        rightExpression: rhs,
                                                        modifier: NSComparisonPredicate.Modifier.any,
                                                        type: NSComparisonPredicate.Operator.notEqualTo,
                                                        options: NSComparisonPredicate.Options.diacriticInsensitive)
        
        return q
    }

    class func notEqualToPredicate(key: String, value: Any) -> NSPredicate {
        let lhs: NSExpression = NSExpression.init(forKeyPath: key)
        let rhs: NSExpression = NSExpression.init(forConstantValue: value)
        let q: NSPredicate = NSComparisonPredicate.init(leftExpression: lhs,
                                                        rightExpression: rhs,
                                                        modifier: NSComparisonPredicate.Modifier.direct,
                                                        type: NSComparisonPredicate.Operator.notEqualTo,
                                                        options: NSComparisonPredicate.Options.diacriticInsensitive)
        
        return q
    }
/*
 - (NSPredicate*)inWithPredicateForKey:(NSString*)key andValues:(NSArray*)values
 {
 return [NSPredicate predicateWithFormat: @"(%K IN %@)", key, values];
 }

 */
    class func inArrayForKey(_ key: String, values: [NSManagedObject]) -> NSPredicate {
        return NSPredicate.init(format: "(%K in %@)", key, values)
    }
    
    class func inSetForKey(_ key: String, values: Set<NSManagedObject>) -> NSPredicate {
        return NSPredicate.init(format: "(%K in %@)", key, values)
    }
    
    class func containsTypePredicate(key: String, value: Any) -> NSPredicate {
        let lhs: NSExpression = NSExpression.init(forKeyPath: key)
        let rhs: NSExpression = NSExpression.init(forConstantValue: value)
        let q: NSPredicate = NSComparisonPredicate.init(leftExpression: lhs,
                                                        rightExpression: rhs,
                                                        modifier: NSComparisonPredicate.Modifier.direct,
                                                        type: NSComparisonPredicate.Operator.contains,
                                                        options: NSComparisonPredicate.Options.caseInsensitive)

        return q
    }
    
    class func greaterThanPredicate(key: String, value: Any) -> NSPredicate {
        let lhs: NSExpression = NSExpression.init(forKeyPath: key)
        let rhs: NSExpression = NSExpression.init(forConstantValue: value)
        let q: NSPredicate = NSComparisonPredicate.init(leftExpression: lhs,
                                                        rightExpression: rhs,
                                                        modifier: NSComparisonPredicate.Modifier.direct,
                                                        type: NSComparisonPredicate.Operator.greaterThan,
                                                        options: NSComparisonPredicate.Options.normalized)
        
        return q
    }
    
    class func greaterThanOrEqualToPredicate(key: String, value: Any) -> NSPredicate {
        let lhs: NSExpression = NSExpression.init(forKeyPath: key)
        let rhs: NSExpression = NSExpression.init(forConstantValue: value)
        let q: NSPredicate = NSComparisonPredicate.init(leftExpression: lhs,
                                                        rightExpression: rhs,
                                                        modifier: NSComparisonPredicate.Modifier.direct,
                                                        type: NSComparisonPredicate.Operator.greaterThanOrEqualTo,
                                                        options: NSComparisonPredicate.Options.normalized)
        
        return q
    }
    
    class func lessThanPredicate(key: String, value: Any) -> NSPredicate {
        let lhs: NSExpression = NSExpression.init(forKeyPath: key)
        let rhs: NSExpression = NSExpression.init(forConstantValue: value)
        let q: NSPredicate = NSComparisonPredicate.init(leftExpression: lhs,
                                                        rightExpression: rhs,
                                                        modifier: NSComparisonPredicate.Modifier.direct,
                                                        type: NSComparisonPredicate.Operator.lessThan,
                                                        options: NSComparisonPredicate.Options.normalized)
        
        return q
    }
    
    class func lessThanOrEqualToPredicate(key: String, value: Any) -> NSPredicate {
        let lhs: NSExpression = NSExpression.init(forKeyPath: key)
        let rhs: NSExpression = NSExpression.init(forConstantValue: value)
        let q: NSPredicate = NSComparisonPredicate.init(leftExpression: lhs,
                                                        rightExpression: rhs,
                                                        modifier: NSComparisonPredicate.Modifier.direct,
                                                        type: NSComparisonPredicate.Operator.lessThanOrEqualTo,
                                                        options: NSComparisonPredicate.Options.normalized)
        
        return q
    }
    
    /**
     This is an interesting predicate but is not supported by core data
     https://stackoverflow.com/questions/5268272/nspredicate-between-with-nsdate-causes-nsdate-constantvalue-unrecognized-s
     Gonna keep it anyway
    */
    class func between(_ values: [AnyObject], forKey: String) -> NSPredicate {
        let q = NSPredicate.init(format: "%K BETWEEN %@", forKey, values)
        return q
    }
}
