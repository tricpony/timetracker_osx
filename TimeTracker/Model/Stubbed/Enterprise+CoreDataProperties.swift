//
//  Enterprise+CoreDataProperties.swift
//  
//
//  Created by aarthur on 2/2/19.
//
//

import Foundation
import CoreData


extension Enterprise {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Enterprise> {
        return NSFetchRequest<Enterprise>(entityName: "Enterprise")
    }

    @NSManaged public var name: String?
    @NSManaged public var logo: NSData?
    @NSManaged public var clients: NSSet?
    @NSManaged public var imtIdentifier: String?
}

// MARK: Generated accessors for clients
extension Enterprise {

    @objc(addClientsObject:)
    @NSManaged public func addToClients(_ value: Client)

    @objc(removeClientsObject:)
    @NSManaged public func removeFromClients(_ value: Client)

    @objc(addClients:)
    @NSManaged public func addToClients(_ values: NSSet)

    @objc(removeClients:)
    @NSManaged public func removeFromClients(_ values: NSSet)

}
