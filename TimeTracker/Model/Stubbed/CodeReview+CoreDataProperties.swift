//
//  CodeReview+CoreDataProperties.swift
//  
//
//  Created by aarthur on 2/15/19.
//
//

import Foundation
import CoreData


extension CodeReview {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CodeReview> {
        return NSFetchRequest<CodeReview>(entityName: "CodeReview")
    }

    @NSManaged public var jiraIdentifier: String?
    @NSManaged public var timeSlip: TimeSlip?

}
