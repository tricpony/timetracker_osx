//
//  WorkSlip+CoreDataProperties.swift
//  
//
//  Created by aarthur on 2/15/19.
//
//

import Foundation
import CoreData


extension WorkSlip {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WorkSlip> {
        return NSFetchRequest<WorkSlip>(entityName: "WorkSlip")
    }

    @NSManaged public var didPullRequest: Bool
    @NSManaged public var jiraIdentifier: String?
    @NSManaged public var sprint: String?
    @NSManaged public var about: String?
    @NSManaged public var status: String?
    @NSManaged public var timeSlip: TimeSlip?

}
