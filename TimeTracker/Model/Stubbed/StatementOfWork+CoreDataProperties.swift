//
//  StatementOfWork+CoreDataProperties.swift
//  
//
//  Created by aarthur on 2/2/19.
//
//

import Foundation
import CoreData


extension StatementOfWork {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StatementOfWork> {
        return NSFetchRequest<StatementOfWork>(entityName: "StatementOfWork")
    }

    @NSManaged public var name: String?
    @NSManaged public var starting: NSDate?
    @NSManaged public var ending: NSDate?
    @NSManaged public var client: Client?
    @NSManaged public var project: Project?
    @NSManaged public var rate: NSDecimalNumber?
    @NSManaged public var imtIdentifier: String?
}
