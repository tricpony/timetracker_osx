//
//  Invoice+CoreDataProperties.swift
//
//
//  Created by aarthur on 2/5/19.
//
//

import Foundation
import CoreData


extension Invoice {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Invoice> {
        return NSFetchRequest<Invoice>(entityName: "Invoice")
    }
    
    @NSManaged public var billed: Bool
    @NSManaged public var paid: Bool
    @NSManaged public var project: Project?
    @NSManaged public var timeSlips: NSSet?

}

// MARK: Generated accessors for timeSlips
extension Invoice {
    
    @objc(addTimeSlipsObject:)
    @NSManaged public func addToTimeSlips(_ value: TimeSlip)
    
    @objc(removeTimeSlipsObject:)
    @NSManaged public func removeFromTimeSlips(_ value: TimeSlip)
    
    @objc(addTimeSlips:)
    @NSManaged public func addToTimeSlips(_ values: NSSet)
    
    @objc(removeTimeSlips:)
    @NSManaged public func removeFromTimeSlips(_ values: NSSet)
    
}
