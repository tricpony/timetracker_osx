//
//  ConfigRelationship+CoreDataProperties.swift
//  
//
//  Created by aarthur on 2/19/19.
//
//

import Foundation
import CoreData


extension ConfigRelationship {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ConfigRelationship> {
        return NSFetchRequest<ConfigRelationship>(entityName: "ConfigRelationship")
    }

    @NSManaged public var cardinality: String?
    @NSManaged public var hidden: Bool
    @NSManaged public var mapsTo: String?
    @NSManaged public var name: String?
    @NSManaged public var templateSubstitution: String?
    @NSManaged public var valueKeyPath: String?
    @NSManaged public var rootObjectKeyPath: String?
    @NSManaged public var configuration: Configuration?

}
