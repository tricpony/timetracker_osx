//
//  Project+CoreDataProperties.swift
//
//
//  Created by aarthur on 2/3/19.
//
//

import Foundation
import CoreData


extension Project {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Project> {
        return NSFetchRequest<Project>(entityName: "Project")
    }
    
    @NSManaged public var name: String?
    @NSManaged public var client: Client?
    @NSManaged public var invoices: NSSet?
    @NSManaged public var statementOfwork: StatementOfWork?
    @NSManaged public var rate: NSDecimalNumber?
    @NSManaged public var imtIdentifier: String?
}

// MARK: Generated accessors for invoices
extension Project {
    
    @objc(addInvoicesObject:)
    @NSManaged public func addToInvoices(_ value: Invoice)
    
    @objc(removeInvoicesObject:)
    @NSManaged public func removeFromInvoices(_ value: Invoice)
    
    @objc(addInvoices:)
    @NSManaged public func addToInvoices(_ values: NSSet)
    
    @objc(removeInvoices:)
    @NSManaged public func removeFromInvoices(_ values: NSSet)
    
}
