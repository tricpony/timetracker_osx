//
//  Configuration+CoreDataProperties.swift
//  
//
//  Created by aarthur on 2/19/19.
//
//

import Foundation
import CoreData


extension Configuration {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Configuration> {
        return NSFetchRequest<Configuration>(entityName: "Configuration")
    }

    @NSManaged public var durationTemplateSubstitution: String?
    @NSManaged public var durationValueKeyPath: String?
    @NSManaged public var nextIMT_ID: Int32
    @NSManaged public var order: Int16
    @NSManaged public var identifier: String?
    @NSManaged public var objectType: String?
    @NSManaged public var startingDateTemplateSubstitution: String?
    @NSManaged public var startingDateValueKeyPath: String?
    @NSManaged public var template: String?
    @NSManaged public var templateSubstitution: String?
    @NSManaged public var valueKeyPath: String?
    @NSManaged public var rootObjectKeyPath: String?
    @NSManaged public var relationships: NSSet?
    @NSManaged public var version: ConfigVersion?
    
}

// MARK: Generated accessors for relationships
extension Configuration {
    
    @objc(addRelationshipsObject:)
    @NSManaged public func addToRelationships(_ value: ConfigRelationship)
    
    @objc(removeRelationshipsObject:)
    @NSManaged public func removeFromRelationships(_ value: ConfigRelationship)
    
    @objc(addRelationships:)
    @NSManaged public func addToRelationships(_ values: NSSet)
    
    @objc(removeRelationships:)
    @NSManaged public func removeFromRelationships(_ values: NSSet)
    
}
