//
//  TimeSlip+CoreDataProperties.swift
//
//
//  Created by aarthur on 2/15/19.
//
//

import Foundation
import CoreData


extension TimeSlip {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<TimeSlip> {
        return NSFetchRequest<TimeSlip>(entityName: "TimeSlip")
    }
    
    @NSManaged public var duration: Float
    @NSManaged public var notes: String?
    @NSManaged public var startingDate: NSDate?
    @NSManaged public var invoice: Invoice?
    @NSManaged public var workSlips: NSSet?
    @NSManaged public var codeReviews: NSSet?
    @NSManaged public var imtIdentifier: String?
}

// MARK: Generated accessors for workSlips
extension TimeSlip {
    
    @objc(addWorkSlipsObject:)
    @NSManaged public func addToWorkSlips(_ value: WorkSlip)
    
    @objc(removeWorkSlipsObject:)
    @NSManaged public func removeFromWorkSlips(_ value: WorkSlip)
    
    @objc(addWorkSlips:)
    @NSManaged public func addToWorkSlips(_ values: NSSet)
    
    @objc(removeWorkSlips:)
    @NSManaged public func removeFromWorkSlips(_ values: NSSet)
    
}

// MARK: Generated accessors for codeReviews
extension TimeSlip {
    
    @objc(addCodeReviewsObject:)
    @NSManaged public func addToCodeReviews(_ value: CodeReview)
    
    @objc(removeCodeReviewsObject:)
    @NSManaged public func removeFromCodeReviews(_ value: CodeReview)
    
    @objc(addCodeReviews:)
    @NSManaged public func addToCodeReviews(_ values: NSSet)
    
    @objc(removeCodeReviews:)
    @NSManaged public func removeFromCodeReviews(_ values: NSSet)
    
}
