//
//  Client+CoreDataProperties.swift
//  
//
//  Created by aarthur on 2/2/19.
//
//

import Foundation
import CoreData


extension Client {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Client> {
        return NSFetchRequest<Client>(entityName: "Client")
    }

    @NSManaged public var name: String?
    @NSManaged public var imtIdentifier: String?
    @NSManaged public var projects: NSSet?
    @NSManaged public var enterprise: Enterprise?
    @NSManaged public var statementsOfWork: NSSet?
}

// MARK: Generated accessors for projects
extension Client {

    @objc(addProjectsObject:)
    @NSManaged public func addToProjects(_ value: Project)

    @objc(removeProjectsObject:)
    @NSManaged public func removeFromProjects(_ value: Project)

    @objc(addProjects:)
    @NSManaged public func addToProjects(_ values: NSSet)

    @objc(removeProjects:)
    @NSManaged public func removeFromProjects(_ values: NSSet)

}

// MARK: Generated accessors for statementsOfWork
extension Client {

    @objc(addStatementsOfWorkObject:)
    @NSManaged public func addToStatementsOfWork(_ value: StatementOfWork)

    @objc(removeStatementsOfWorkObject:)
    @NSManaged public func removeFromStatementsOfWork(_ value: StatementOfWork)

    @objc(addStatementsOfWork:)
    @NSManaged public func addToStatementsOfWork(_ values: NSSet)

    @objc(removeStatementsOfWork:)
    @NSManaged public func removeFromStatementsOfWork(_ values: NSSet)

}
