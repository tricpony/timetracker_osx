//
//  ConfigVersion+CoreDataProperties.swift
//  
//
//  Created by aarthur on 2/19/19.
//
//

import Foundation
import CoreData


extension ConfigVersion {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ConfigVersion> {
        return NSFetchRequest<ConfigVersion>(entityName: "ConfigVersion")
    }

    @NSManaged public var version: String?
    @NSManaged public var header: String?
    @NSManaged public var configurations: NSSet?
    
}

// MARK: Generated accessors for configurations
extension ConfigVersion {
    
    @objc(addConfigurationsObject:)
    @NSManaged public func addToConfigurations(_ value: Configuration)
    
    @objc(removeConfigurationsObject:)
    @NSManaged public func removeFromConfigurations(_ value: Configuration)
    
    @objc(addConfigurations:)
    @NSManaged public func addToConfigurations(_ values: NSSet)
    
    @objc(removeConfigurations:)
    @NSManaged public func removeFromConfigurations(_ values: NSSet)
    
}
