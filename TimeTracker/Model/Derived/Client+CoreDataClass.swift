//
//  Client+CoreDataClass.swift
//  
//
//  Created by aarthur on 2/2/19.
//
//

import Foundation
import CoreData

@objc(Client)
public class Client: BaseManagedObject, NSCoding, OutlineViewArchivable {
    lazy var archivableDate: Date = {
        guard let date = self.createDate else { return Date() }
        return date as Date
    }()

    // MARK: - Importable
    
    @objc override var importableName: String? {
        get {
            return name
        }
        
        set (newValue) {
            name = newValue
        }
    }

    func archivableMatchesObject(_ value: OutlineViewArchivable) -> Bool {
        return archivableDate == value.archivableDate
    }

    required convenience public init?(coder decoder: NSCoder) {
        guard let createDate = decoder.decodeObject(forKey: "createDate") as? Date,
            let entity = NSEntityDescription.entity(forEntityName: "Client", in: NSManagedObjectContext.mr_default())
            else { return nil }
        
        self.init(entity: entity, insertInto: nil)
        self.createDate = createDate as NSDate
    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(self.createDate, forKey: "createDate")
    }

    class func createClient(in ctx: NSManagedObjectContext, enterprise: Enterprise, name: String) -> Client? {
        if let client = Client.mr_createEntity(in: ctx) {
            if let _ = StatementOfWork.createSOW(in: ctx, forClient: client, name: "statement of work name") {
                client.name = name
                enterprise.addToClients(client)
                return client
            }
        }
        
        return nil
    }
    
    func clone(ctx: NSManagedObjectContext, sow: StatementOfWork) -> Client? {
        guard let clonedClient = Client.mr_createEntity(in: ctx) else { return nil }
        guard let clonedEnterprise = Enterprise.mr_createEntity(in: ctx) else { return nil }
        guard let clonedSOW = StatementOfWork.mr_createEntity(in: ctx) else { return nil }
        
        self.copyAttributes(to: clonedClient)
        self.enterprise?.copyAttributes(to: clonedEnterprise)
        sow.copyAttributes(to: clonedSOW)
        
        //assemble them
        clonedEnterprise.addToClients(clonedClient)
        clonedClient.addToStatementsOfWork(clonedSOW)

        return clonedClient
    }
    
    func allSOW_validForSave() -> Bool {
        for sow in statementsOfWork! {
            let nextSOW = sow as! StatementOfWork
            if !nextSOW.isValidForSave() {
                return false
            }
        }
        return true
    }
    
    // MARK: - ConfigurationKeyValueCompliant
        
    override var clientJobIds: String {
        let ctx = self.managedObjectContext
        let configRel = CoreDataUtility.fetchConfigRelationship(in: ctx!, configCless: "Client", templateSubstitution: "$JOB$")
        return configRel?.cardinality ?? ""
    }

    // MARK: - IMT Export
    
    override func exportConfig(_ config: Configuration, invoice: Invoice) -> String {
        let id_substitution = "$IDENTIFIER$"
        guard let template = config.template else { return "" }
        var buffer = ""
        var properID: String? {
            if imtIdentifier == nil {
                guard let id = self.createNextImtIdentifier as String? else { return "" }
                imtIdentifier = id
            }
            return imtIdentifier
        }
        
        guard let id = properID else { return "" }
        guard let substitution = config.templateSubstitution else { return "" }
        guard let key = config.valueKeyPath else { return "" }
        guard let value = self.value(forKeyPath: key) as? String else { return "" }

        buffer.append(contentsOf: (template.findAndReplacement(of: [id_substitution:id, substitution:value])))
        if let safeConfigRel = config.relationships {
            guard let ctx = self.managedObjectContext else { return "Export Failed, no managed object context" }
            let activityConfig = CoreDataUtility.fetchConfiguration(in: ctx, matcingClassName: "_activity")
            for nextRel in safeConfigRel {
                guard let safeRel = nextRel as? ConfigRelationship else { continue }
                guard let substitution = safeRel.templateSubstitution else { continue }
                if let safeClassname = safeRel.mapsTo, safeClassname == "_activity" {
                    guard let objValue = activityConfig?.nextIMT_ID else { continue }
                    let value = "z\(objValue)"
                    buffer = buffer.findAndReplacement(of: [substitution:value])
                    continue
                }

                guard let rootObjectKey = safeRel.rootObjectKeyPath else { continue }
                guard let rootObject = invoice.value(forKeyPath: rootObjectKey) as! BaseManagedObject? else { continue }
                guard let key = safeRel.valueKeyPath else { continue }
                guard let value = rootObject.value(forKey: key) as? String else { continue }
                buffer = buffer.findAndReplacement(of: [substitution:value])
            }
        }
        
        return buffer
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//
/**
 This is here to help the outline view & the tree controller to work together
 These keys are set in the attributes inspector of the NSTreeController object
 **/
extension Client: OutlineViewable {
    var isLeaf: Bool {
        return statementsOfWork?.count == 0
    }
    
    var outlineCount: Int {
        if let safeCount = statementsOfWork?.count {
            return safeCount
        }
        return 0
    }
    
    var outlineChildren: [OutlineViewable] {
        return statementsOfWork?.allObjects as! [StatementOfWork]
    }
}
//
//////////////////////////////////////////////////////////////////////////////////////////////////
