//
//  Project+CoreDataClass.swift
//  
//
//  Created by aarthur on 2/2/19.
//
//

import Foundation
import CoreData

@objc(Project)
public class Project: BaseManagedObject, NSCoding, OutlineViewArchivable {
    var selectedObjectsObservation: NSKeyValueObservation?
    lazy var archivableDate: Date = {
        guard let date = self.createDate else { return Date() }
        return date as Date
    }()

    // MARK: - Importable
    
    @objc override var importableName: String? {
        get {
            return name
        }
        
        set (newValue) {
            name = newValue
        }
    }

    override func assemble(_ objects: [String : Any], isLive: Bool) {
        guard let enterprise = objects["Enterprise"] as? Enterprise else { return }
        guard let client = objects["Client"] as? Client else { return }
        guard let sow = objects["StatementOfWork"] as? StatementOfWork else { return }
        guard let time = objects["timeslips"] as? [TimeSlip] else { return }
        guard let ctx = self.managedObjectContext else { return }

        enterprise.addToClients(client)
        client.addToStatementsOfWork(sow)
        statementOfwork = sow
        self.client = client
        
        if time.count > 0 {
            guard let invoice = Invoice.createInvoice(in: ctx, project: self) else { return }
            self.addToInvoices(invoice)

            for nextTime in time {
                nextTime.initializeWithTrackingArtifacts(in: ctx)
                invoice.addToTimeSlips(nextTime)
            }
        }
    }

    func archivableMatchesObject(_ value: OutlineViewArchivable) -> Bool {
        return self.createDate == value.archivableDate as NSDate
        
        //something wrong here archivableDate is returning a date in 2001, need to investigate
//        return archivableDate == value.archivableDate
    }

    required convenience public init?(coder decoder: NSCoder) {
        guard let createDate = decoder.decodeObject(forKey: "createDate") as? Date,
            let entity = NSEntityDescription.entity(forEntityName: "Project", in: NSManagedObjectContext.mr_default())
        else { return nil }
        
        self.init(entity: entity, insertInto: nil)
        self.createDate = createDate as NSDate
    }

    public func encode(with coder: NSCoder) {
        coder.encode(self.createDate, forKey: "createDate")
    }

    class func createProject(in ctx: NSManagedObjectContext, client: Client, sow: StatementOfWork,  name: String = "Project Name") -> Project? {
        if let project = Project.mr_createEntity(in: ctx) {
            if let _ = Invoice.createInvoice(in: ctx, project: project) {
                project.name = name
                project.client = client
                project.statementOfwork = sow
                sow.project = project
                return project
            }
        }
        
        return nil
    }
    
    func hasStatmentOfWork() -> Bool {
        return statementOfwork != nil
    }
    
    override func flattenedTimeSlips() -> [TimeSlip] {
        if let safeCtx = self.managedObjectContext {
            let timeSlips = CoreDataUtility.fetchTimeSlipsForProject(in: safeCtx, project: self)
            return timeSlips
        }
        return [TimeSlip]()
    }
    
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        rate = 95.0
    }
    
    func moveTimeSlipsHere(timeSlips: [TimeSlip]) {
        if timeSlips.count > 0 {
            guard let ctx = managedObjectContext else { return }
            if let newInvoice = Invoice.createInvoice(in: ctx, project: self) {
                self.addToInvoices(newInvoice);
                for nextTime in timeSlips {
                    newInvoice.addToTimeSlips(nextTime)
                }
            }
        }
    }
    
    func orderedInvoices() -> [Invoice] {
        if let safeInvoices = invoices {
            let sorter = NSSortDescriptor.init(key: "createDate", ascending: false)
            if let items = safeInvoices.allObjects as NSArray? {
                let orderedItems = items.sortedArray(using: [sorter])
                return orderedItems as! [Invoice]
            }
        }
        return [Invoice]()
    }
    
    func unBilledInvoice() -> Invoice? {
        if let invoice = orderedInvoices().last {
            return invoice
        }
        return nil
    }
    
    // MARK: - IMT Export

    override func exportConfig(_ config: Configuration, invoice: Invoice) -> String {
        let id_substitution = "$IDENTIFIER$"
        guard let template = config.template else { return "" }
        var buffer = ""
        var properID: String? {
            if imtIdentifier == nil {
                guard let id = self.createNextImtIdentifier as String? else { return "" }
                imtIdentifier = id
            }
            return imtIdentifier
        }
        
        guard let id = properID else { return "" }
        guard let substitution = config.templateSubstitution else { return "" }
        guard let key = config.valueKeyPath else { return "" }
        guard let value = self.value(forKeyPath: key) as? String else { return "" }
        
        buffer.append(contentsOf: (template.findAndReplacement(of: [id_substitution:id, substitution:value])))
        if let safeConfigRel = config.relationships {
            guard let ctx = self.managedObjectContext else { return "Export Failed, no managed object context" }
            let activityConfig = CoreDataUtility.fetchConfiguration(in: ctx, matcingClassName: "_activity")
            for nextRel in safeConfigRel {
                guard let safeRel = nextRel as? ConfigRelationship else { continue }
                guard let substitution = safeRel.templateSubstitution else { continue }
                if let safeClassname = safeRel.mapsTo, safeClassname == "_activity" {
                    guard let objValue = activityConfig?.nextIMT_ID else { continue }
                    let value = "z\(objValue)"
                    buffer = buffer.findAndReplacement(of: [substitution:value])
                    continue
                }
                
                guard let rootObjectKey = safeRel.rootObjectKeyPath else { continue }
                guard let rootObject = invoice.value(forKeyPath: rootObjectKey) as! BaseManagedObject? else { continue }
                guard let key = safeRel.valueKeyPath else { continue }
                guard let value = rootObject.value(forKey: key) as? String else { continue }
                buffer = buffer.findAndReplacement(of: [substitution:value])
            }
        }
        return buffer
    }

    // MARK: - NSKeyValueObservation
    
    func registerProjectRelationships() {
        selectedObjectsObservation = observe(
            \.statementOfwork,
            options: [.old, .new]
        ) { [weak self] object, change in
            guard let strongSelf = self else { return }
            
            let sow = strongSelf.statementOfwork
            if sow == nil {
                print("vital relationship is lost %@", "statementOfWork")
                fatalError()
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//
/**
 This is here to help the outline view & the tree controller to work together
 These keys are set in the attributes inspector of the NSTreeController object
 **/
extension Project: OutlineViewable {
    var isLeaf: Bool {
        return invoices?.count == 0
    }
    
    var outlineCount: Int {
        if let safeCount = invoices?.count {
            return safeCount
        }
        return 0
    }
    
    var outlineChildren: [OutlineViewable] {
        return invoices?.allObjects as! [OutlineViewable]
    }
}
//
//////////////////////////////////////////////////////////////////////////////////////////////////

