//
//  Invoice+CoreDataClass.swift
//  
//
//  Created by aarthur on 2/2/19.
//
//

import Foundation
import CoreData

enum ReportType {
    case csv
    case imt
}

enum CSVColumn: String, CaseIterable {
    case date = "Date"
    case sow = "Engagement"
    case sprint = "Sprint"
    case duration = "Duration"
    case activity = "Activity"
    
    //should not need to do this, but the compiler is a bitch!!
    static var allCases: [CSVColumn] {
        return [.date, .sow, .sprint, .duration, .activity]
    }

    static func columnLabels() -> String {
        let delimiter = "\t"
        var buffer = ""
        CSVColumn.allCases.forEach {
            let label: String = $0.rawValue
            buffer = buffer.appendingFormat("%@\(delimiter)", label)
        }
        let index = buffer.index(buffer.endIndex, offsetBy: -1)
        buffer = String(buffer.prefix(upTo: index))
        return buffer
    }
}

@objc protocol OutlineViewBindable:class {
    var name: String { get }
}

@objc protocol InvoiceKeyValueCompliant:class {
    var totalHours: Float { get }
    var totalDollars: Float { get }
    var totalGrossDollars: Float { get }
    var deficitHours: Float { get }
    var statusDisplayValue: String { get }
    var statusDisplayValueColor: NSColor { get }
}

enum StatusDisplayChoice: String {
    case ahead = "Ahead"
    case behind = "Behind"
    case even = "Even"
    
    static func displayChoice(for deficitHours: Float) -> StatusDisplayChoice {
        if deficitHours == 0 {
            return .even
        }
        else if deficitHours < 0 {
            return .behind
        }
        return .ahead
    }
    
    func displayColor() -> NSColor {
        switch self {
        case .behind:
            return NSColor.fireBrick
        case .ahead:
            return NSColor.darkGreen
        default:
            return NSColor.black
        }
    }
}

@objc(Invoice)
public class Invoice: BaseManagedObject, NSCoding, OutlineViewArchivable, InvoiceKeyValueCompliant {
    lazy var dateFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "MMMM"
        return formatter
    }()
    lazy var exportDateFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy"
        return formatter
    }()
    lazy var archivableDate: Date = {
        guard let date = self.createDate else { return Date() }
        return date as Date
    }()

    var totalHours: Float {
        var sum: Float = 0
        if let safeTimeSlips = timeSlips {
            for time in safeTimeSlips {
                if let nextTime = time as? TimeSlip {
                    sum += nextTime.duration
                }
            }
        }
        return sum
    }
    
    var totalDollars: Float {
        let hours = totalHours
        guard let safeProject = project else { return 0 }
        guard let rate = safeProject.rate else { return 0 }
        return hours * Float(truncating: rate)
    }
    
    var totalGrossDollars: Float {
        guard let safeSlips = timeSlips else { return totalDollars }
        guard let anySlip = safeSlips.anyObject() as? TimeSlip else { return totalDollars }
        guard let dayOfWork = anySlip.startingDate else { return totalDollars }
        guard let ctx = self.managedObjectContext else { return totalDollars }
        guard let start = dayOfWork.startOfMonth() else { return totalDollars }
        guard let end = dayOfWork.endOfMonth() else { return totalDollars }
        guard let timeSlip = CoreDataUtility.fetchAnyTimeslip(in: ctx, betweenDates: [start, end], anti:self) else { return totalDollars }
        guard let otherInvoice = timeSlip.invoice else { return totalDollars }

        return totalDollars + otherInvoice.totalDollars
    }
    
    var deficitHours: Float {
        let defaultDeficit: Float = 0
        guard let safeSlips = timeSlips else { return defaultDeficit }
        guard let anySlip = safeSlips.anyObject() as? TimeSlip else { return defaultDeficit }
        guard let dayOfWork = anySlip.startingDate else { return defaultDeficit }
        guard let ctx = self.managedObjectContext else { return defaultDeficit }
        guard let start = dayOfWork.startOfMonth() else { return defaultDeficit }
        guard let end = dayOfWork.endOfMonth() else { return defaultDeficit }
        let baseDate = isCurrentMonth() ? NSDate() : end as NSDate
        let weekDayCount = baseDate.weekDayCountOfMonthSoFar()
        let fullWorkLoad = weekDayCount * 8;
        if let timeSlip = CoreDataUtility.fetchAnyTimeslip(in: ctx, betweenDates: [start, end], anti:self) {
            guard let otherInvoice = timeSlip.invoice else { return totalDollars }
            return (totalHours + otherInvoice.totalHours) - Float(fullWorkLoad)
        }
        return totalHours - Float(fullWorkLoad)
    }
    
    var statusDisplayValue: String {
        return StatusDisplayChoice.displayChoice(for: deficitHours).rawValue
    }
    
    var statusDisplayValueColor: NSColor {
        let status = StatusDisplayChoice.displayChoice(for: deficitHours)
        return status.displayColor()
    }
    
    func archivableMatchesObject(_ value: OutlineViewArchivable) -> Bool {
        return archivableDate == value.archivableDate
    }

    required convenience public init?(coder decoder: NSCoder) {
        guard let createDate = decoder.decodeObject(forKey: "createDate") as? Date,
            let entity = NSEntityDescription.entity(forEntityName: "Invoice", in: NSManagedObjectContext.mr_default())
            else { return nil }
        
        self.init(entity: entity, insertInto: nil)
        self.createDate = createDate as NSDate
    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(self.createDate, forKey: "createDate")
    }

    class func createInvoice(in ctx: NSManagedObjectContext, project: Project) -> Invoice? {
        let invoice = Invoice.mr_createEntity(in: ctx)
        invoice?.project = project
        
        return invoice
    }

    override func flattenedTimeSlips() -> [TimeSlip] {
        if let safeTimeSlips = timeSlips {
            return safeTimeSlips.allObjects as! [TimeSlip]
        }
        return [TimeSlip]()
    }

    func isCurrentMonth() -> Bool {
        guard let safeSlips = timeSlips else { return false }
        guard let anySlip = safeSlips.anyObject() as? TimeSlip else { return false }
        guard let dayOfWork = anySlip.startingDate else { return false }
        return NSDate().isInCurrentMonth(date: dayOfWork)
    }
    
    func orderedTimeSlips() -> [TimeSlip] {
        guard let safeSlips = timeSlips else { return [] }
        let desc = NSSortDescriptor(key: "startingDate", ascending: true)

        return safeSlips.sortedArray(using: [desc]) as! [TimeSlip]
    }

    // MARK: - IMT Export
    
    func fillDocumentNode(from: Configuration) -> String {
        guard let template = from.template else { return "" }
        var buffer = ""
        guard let id_substitution = from.templateSubstitution else { return "" }
        guard let key = from.valueKeyPath else { return "" }
        guard let valueObject = from.value(forKeyPath: key) as? Int32 else { return "" }
        let value = "z\(valueObject)"

        buffer.append(contentsOf: (template.findAndReplacement(of: [id_substitution:value])))
        if let safeConfigRel = from.relationships {
            guard let ctx = self.managedObjectContext else { return "Export Failed, no managed object context" }

            for nextRel in safeConfigRel {
                guard let safeRel = nextRel as? ConfigRelationship else { continue }
                guard let substitution = safeRel.templateSubstitution else { continue }
                guard let fetchArg = safeRel.rootObjectKeyPath else { continue }
                let argConfig = CoreDataUtility.fetchConfiguration(in: ctx, matcingClassName: fetchArg)
                guard let objValue = argConfig?.nextIMT_ID else { continue }
                let value = "z\(objValue)"
                buffer = buffer.findAndReplacement(of: [substitution:value])
            }
        }
        return buffer
    }
    
    func fillActivityNode(from: Configuration) -> String {
        guard let template = from.template else { return "" }
        var buffer = ""
        guard let id_substitution = from.templateSubstitution else { return "" }
        guard let key = from.valueKeyPath else { return "" }
        guard let valueObject = from.value(forKeyPath: key) as? Int32 else { return "" }
        let value = "z\(valueObject)"
        
        buffer.append(contentsOf: (template.findAndReplacement(of: [id_substitution:value])))
        if let safeConfigRel = from.relationships {
            for nextRel in safeConfigRel {
                guard let safeRel = nextRel as? ConfigRelationship else { continue }
                guard let rootObjectKey = safeRel.rootObjectKeyPath else { continue }
                guard let substitution = safeRel.templateSubstitution else { continue }
                guard let key = safeRel.valueKeyPath else { continue }
                if rootObjectKey != "self" {
                    guard let rootObject = self.value(forKeyPath: rootObjectKey) as! BaseManagedObject? else { continue }
                    guard let value = rootObject.value(forKey: key) as? String else { continue }
                    buffer = buffer.findAndReplacement(of: [substitution:value])
                }
                else{
                    if let safeTimeslips = timeSlips {
                        let finalResult = safeTimeslips.reduce("") {
                            (partialResult, next) -> String in
                            if let imt_id = (next as! TimeSlip).value(forKeyPath: key) {
                                return "\(partialResult) \(imt_id)"
                            }
                            return ""
                        }
                        let validationStatus = validateTimeSlipIDs(finalResult)
                        if validationStatus.status == true {
                            buffer = buffer.findAndReplacement(of: [substitution:finalResult.trim()])
                        }
                        else {
                            let ctx = self.managedObjectContext
                            let offendingTimeSlips = CoreDataUtility.fetchTimeSlip(invoice: self, in: ctx!)
                            
                            //reset them all!! damned duplicate ids
                            for offendingSlip in offendingTimeSlips {
                                guard let id = offendingSlip.createNextImtIdentifier as String? else { return "" }
                                offendingSlip.imtIdentifier = id
                            }
                            ctx?.mr_saveToPersistentStoreAndWait()
                            let finalResult = offendingTimeSlips.reduce("") {
                                (partialResult, next) -> String in
                                if let imt_id = next.value(forKeyPath: key) {
                                    return "\(partialResult) \(imt_id)"
                                }
                                return ""
                            }
                            buffer = buffer.findAndReplacement(of: [substitution:finalResult.trim()])
                        }
                    }
                }
            }
        }

        return buffer
    }

    /**
     Return tuple of false & duplicate if we find any duplicate in ids
    **/
    func validateTimeSlipIDs(_ ids: String) -> (status: Bool, offendingItem: String) {
        let idItems = ids.components(separatedBy: " ")
        var idSet = Set<String>()
        
        for nextItem in idItems {
            let status = idSet.insert(nextItem)
            if status.inserted == false {
                return (status.inserted, status.memberAfterInsert)
            }
        }
        return (true, "")
    }
    
    /**
     Handles all configurations except Activity & Document - which do not map to a model class
    **/
    func fillExplicitConfiguration(_ nextConfig: Configuration, in buffer: String) -> String {
        var realRootObject: BaseManagedObject = self
        guard let rootObjectKey = nextConfig.rootObjectKeyPath else { return "" }
        if rootObjectKey != "self" {
            guard let rootObject = self.value(forKeyPath: rootObjectKey) as! BaseManagedObject? else { return "" }
            realRootObject = rootObject
        }else{
            realRootObject = self
        }
        return buffer.appendingFormat("%@\n", realRootObject.exportConfig(nextConfig, invoice: self))
    }
    
    /**
     Export is driven by Configuration.plist.  Configuration gets loaded into core data mapping to the plist.  When the plist version changes the matching
     model objects are deleted and reloaded.  Those model objects are what actually drive this flow, not the plist itself.  Configuration defines the path
     through model objects such as Enteriprise, Client, Project, StatementOfWork, and TimeSlip to export the data needed to fill an XML string.  The exported
     XML is based on the selected invoice object -- this class.  In the selected invoice, Configuration.rootObjectKeyPath is relative to invoice and
     Configuration.valueKeyPath is relative to whatever root object is.  From Invoice.exportContent the flow is Invoice.fillActivityNode, Invoice.fillDocumentNode,
     Invoice.fillExplicitConfiguration and rootObject.exportConfig.
    **/
    func exportContentAsIMT() -> String {
        guard let ctx = self.managedObjectContext else { return "Export Failed, no managed object context" }
        let configs = CoreDataUtility.fetchAllConfigurations(in: ctx)
        var buffer = ""
        let configVersion = CoreDataUtility.fetchConfigurationVersion(in: ctx)
        guard let header = configVersion?.header else { return "Export Failed, header not found" }

        for nextConfig in configs {
            if let safeClassname = nextConfig.identifier, safeClassname == "_activity" {
                buffer = buffer.appendingFormat("%@\n", fillActivityNode(from: nextConfig))
                
                //the activity node is done, skip to the next one
                continue
            }
            if let safeClassname = nextConfig.identifier, safeClassname == "_document" {
                buffer = buffer.appendingFormat("%@\n", fillDocumentNode(from: nextConfig))

                //the document node is done, skip to the next one
                continue
            }

            buffer = fillExplicitConfiguration(nextConfig, in: buffer)
        }
        return header.appendingFormat("%@</database>", buffer)
    }

    func exportContentAsCSV() -> String {
        let delimiter = "\t"
        let columnLabels = CSVColumn.columnLabels()
        guard let sow = project?.name else { return "" }
        let slips = orderedTimeSlips()
        var buffer = "\(columnLabels)\n"
        slips.forEach {
            let timeSlip = $0
            let date = timeSlip.startingDate
            let displayDate = exportDateFormatter.string(from: date! as Date)
            let sprint = timeSlip.anySprint()
            let duration = timeSlip.duration
            let activityNotes = timeSlip.workSummary()

            //tried to use buffer.appendingFormat() but it was appending (null) at the end - swift bug
            buffer.append("\(displayDate)\(delimiter)")
            buffer.append("\(sow)\(delimiter)")
            buffer.append("\(sprint)\(delimiter)")
            buffer.append("\(duration)\(delimiter)")
            buffer.append("\(activityNotes)\n")
        }
        return buffer
    }

    func exportContent(as type: ReportType = .csv) -> String {
        switch type {
        case .csv:
            return exportContentAsCSV()
        case .imt:
            return exportContentAsIMT()
        }
    }

    override func exportConfig(_ config: Configuration, invoice: Invoice) -> String {
        var buffer = ""

        if let safeTimeSlips = timeSlips {
            for time in safeTimeSlips {
                let nextTime = time as! TimeSlip
                buffer = buffer.appendingFormat("%@\n", nextTime.exportConfig(config, invoice:invoice))
            }
        }

        return buffer
    }
    
    func clone(forSelectedSlips: [TimeSlip], ctx: NSManagedObjectContext) -> Invoice? {
        guard let clonedInvoice = Invoice.mr_createEntity(in: ctx) else { return nil }
        guard let clonedProject = Project.mr_createEntity(in: ctx) else { return nil }
        guard let safeProject = project else { return nil }
        guard let safeSOW = safeProject.statementOfwork else { return nil }
        guard let safeClient = safeProject.client else { return nil }
        guard let clonedClient = safeClient.clone(ctx: ctx, sow: safeSOW) else { return nil }
        guard let clonedSOW = clonedClient.statementsOfWork?.anyObject() as? StatementOfWork else { return nil }
        
        safeProject.copyAttributes(to: clonedProject)
        clonedProject.statementOfwork = clonedSOW
        clonedProject.addToInvoices(clonedInvoice)
        self.copyAttributes(to: clonedInvoice)
        clonedProject.client = clonedClient
        for nextSlip in forSelectedSlips {
            nextSlip.clone(ctx: ctx, invoice: clonedInvoice)
        }
        return clonedInvoice
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//
/**
 This is here to help the outline view & the tree controller to work together
 These keys are set in the attributes inspector of the NSTreeController object
 **/
extension Invoice: OutlineViewable {
    var isLeaf: Bool {
        return true
    }
    
    var outlineCount: Int {
        return 0
    }
    
    var outlineChildren: [OutlineViewable] {
        return [OutlineViewable]()
    }
}

extension Invoice: OutlineViewBindable {
    var name: String {
        let anySlip = timeSlips?.anyObject() as? TimeSlip
        if let safeDateString = anySlip?.startingDate?.asString(formatter: dateFormatter) {
            return safeDateString
        }
        return ""
    }
}
//
//////////////////////////////////////////////////////////////////////////////////////////////////

