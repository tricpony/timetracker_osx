//
//  TimeSlip+CoreDataClass.swift
//  
//
//  Created by aarthur on 2/2/19.
//
//

import Foundation
import CoreData

@objc protocol TimeSlipKeyValueCompliant:class {
    var imtDuration: Float { get }
}

@objc(TimeSlip)
public class TimeSlip: BaseManagedObject, NSCoding, OutlineViewArchivable, TimeSlipKeyValueCompliant {
    lazy var archivableDate: Date = {
        guard let date = self.createDate else { return Date() }
        return date as Date
    }()

    // MARK: - Importable
    
    @objc override var importableName: String? {
        get {
            return notes
        }
        
        set (newValue) {
            notes = newValue
        }
    }

    var imtDuration: Float {
        return durationTimeInSeconds()
    }

    func archivableMatchesObject(_ value: OutlineViewArchivable) -> Bool {
        return archivableDate == value.archivableDate
    }

    required convenience public init?(coder decoder: NSCoder) {
        guard let createDate = decoder.decodeObject(forKey: "createDate") as? Date,
            let entity = NSEntityDescription.entity(forEntityName: "TimeSlip", in: NSManagedObjectContext.mr_default())
            else { return nil }
        
        self.init(entity: entity, insertInto: nil)
        self.createDate = createDate as NSDate
    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(self.createDate, forKey: "createDate")
    }

    @discardableResult class func createTimeSlip(in ctx:NSManagedObjectContext, invoice: Invoice) -> TimeSlip? {
        if let timeSlip = TimeSlip.mr_createEntity(in: ctx) {
            invoice.addToTimeSlips(timeSlip)
            timeSlip.startingDate = Date() as NSDate
            timeSlip.initializeWithTrackingArtifacts(in: ctx)
            return timeSlip
        }
        return nil
    }
    
    func initializeWithTrackingArtifacts(in ctx: NSManagedObjectContext) {
        guard let ticket = WorkSlip.mr_createEntity(in: ctx) else { return }
        guard let codeReview = CodeReview.mr_createEntity(in: ctx) else { return }
        self.addToWorkSlips(ticket)
        self.addToCodeReviews(codeReview)
    }
    
    func clone(ctx: NSManagedObjectContext, invoice: Invoice) {
        guard let clonedSlip = TimeSlip.mr_createEntity(in: ctx) else { return }
        
        self.copyAttributes(to: clonedSlip)
        invoice.addToTimeSlips(clonedSlip)
        guard let safeWorkSlips = workSlips else { return }
        safeWorkSlips.forEach {
            ($0 as! WorkSlip).clone(ctx: ctx, slip: clonedSlip)
        }
        guard let safeCodeReviews = codeReviews else { return }
        safeCodeReviews.forEach {
            ($0 as! CodeReview).clone(ctx: ctx, slip: clonedSlip)
        }
    }
    
    // MARK: - IMT Export

    func anySprint() -> String {
        guard let anySlip = workSlips?.anyObject() as? WorkSlip else { return "Unknown" }
        return anySlip.sprint ?? "Unspecified"
    }
    
    func workSummary(as type: ReportType = .csv) -> String {
        var buffer = ""
        if let safeSlips = workSlips {
            for nextWorkSlip in safeSlips {
                if let safeSlip = nextWorkSlip as? WorkSlip {
                    buffer = buffer.appending(safeSlip.exportWorkSummary())
                }
            }
        }
        if let safeCodeReviews = codeReviews {
            for nextCodeReview in safeCodeReviews {
                if let safeCodeReview = nextCodeReview as? CodeReview {
                    buffer = buffer.appending(safeCodeReview.exportCodeReviewSummary())
                }
            }
        }
        let safeNotes = notes ?? ""
        if safeNotes.count > 0 {
            buffer = buffer.appending("Notes: \(safeNotes.replaceNewLines())")
        }

        return type == .csv ? buffer.replaceNewLines() : buffer
    }
    
    /**
     This does not belong here and is a hack - should be in an extension but when I tried to call it from there it never executed
     Starting to loose faith in Swift
    **/
    func timeIntervalSince2000(in date: NSDate) -> TimeInterval {
        if let century = NSDate.dateOn(year: 2000, month: 1, day: 1) {
            return date.timeIntervalSince(century)
        }
        return 0
    }

    override func exportConfig(_ config: Configuration, invoice: Invoice) -> String {
        let id_substitution = "$IDENTIFIER$"
        guard let template = config.template else { return "" }
        var buffer = ""
        var properID: String? {
            if imtIdentifier == nil {
                guard let id = self.createNextImtIdentifier as String? else { return "" }
                imtIdentifier = id
            }
            return imtIdentifier
        }
        
        guard let id = properID else { return "" }
        guard let substitution = config.templateSubstitution else { return "" }
        guard let timeKey = config.durationValueKeyPath else { return "" }
        guard let time = self.value(forKeyPath: timeKey) else { return "" }
        guard let timeSubstitution = config.durationTemplateSubstitution else { return "" }
        guard let startSubstitution = config.startingDateTemplateSubstitution else { return "" }
        guard let aDate = startingDate else { return "" }
        let start = timeIntervalSince2000(in: aDate)
        let replacePkg = [id_substitution:id,
                          substitution:workSummary(),
                          timeSubstitution:"\(time)",
                          startSubstitution:"\(start)"]

        buffer = buffer.appending(template.findAndReplacement(of: replacePkg))
        if let safeConfigRel = config.relationships {
            guard let ctx = self.managedObjectContext else { return "Export Failed, no managed object context" }
            let activityConfig = CoreDataUtility.fetchConfiguration(in: ctx, matcingClassName: "_activity")
            for nextRel in safeConfigRel {
                guard let safeRel = nextRel as? ConfigRelationship else { continue }
                guard let substitution = safeRel.templateSubstitution else { continue }

                if let safeClassname = safeRel.mapsTo, safeClassname == "_activity" {
                    guard let objValue = activityConfig?.nextIMT_ID else { continue }
                    let value = "z\(objValue)"
                    buffer = buffer.findAndReplacement(of: [substitution:value])
                    continue
                }
                
                guard let rootObjectKey = safeRel.rootObjectKeyPath else { continue }
                guard let rootObject = invoice.value(forKeyPath: rootObjectKey) as! BaseManagedObject? else { continue }
                guard let key = safeRel.valueKeyPath else { continue }
                guard let value = rootObject.value(forKey: key) as? String else { continue }
                buffer = buffer.findAndReplacement(of: [substitution:value])
            }
        }
        
        return buffer
    }
    
    func durationTimeInSeconds() -> Float {
        return duration * 3600
    }
}
