//
//  WorkSlip+CoreDataClass.swift
//  
//
//  Created by aarthur on 2/15/19.
//
//

import Foundation
import CoreData

enum WorkSlipStatus: String {
    case Worked
    case Completed
    case Entered
    case QA_Fail = "QA Fail"

    func exportWorkSummary(jira: String, sprint: String, desc: String? = nil, as type: ReportType = .csv) -> String {
        var buffer = " "
        
        func description(_ desc: String? = nil, as type: ReportType = .csv) -> String {
            var buffer = type == .csv ? "" : "\n"
            
            if let about = desc, about != "Unspecified" {
                buffer = "\(about) "
            }
            return buffer
        }
        
        switch self {
        case .Worked:
            buffer = "Worked ticket \(jira) in sprint \(sprint) \(description(desc))"
        case .Completed:
            buffer = "Completed ticket \(jira) in sprint \(sprint) \(description(desc))"
        case .Entered:
            if sprint.count > 0 {
                buffer = "Entered ticket \(jira) in sprint \(sprint) \(description(desc))"
            }else{
                buffer = "Entered ticket \(jira) \(description(desc))"
            }
        case .QA_Fail:
            buffer = "Worked QA ticket \(jira) in sprint \(sprint) \(description(desc))"
        }

        return buffer
    }
}

protocol Exportable {
    func exportWorkSummary() -> String
}

@objc(WorkSlip)
public class WorkSlip: BaseManagedObject, NSCoding, OutlineViewArchivable, Exportable {
    lazy var archivableDate: Date = {
        guard let date = self.createDate else { return Date() }
        return date as Date
    }()
    
    func archivableMatchesObject(_ value: OutlineViewArchivable) -> Bool {
        return archivableDate == value.archivableDate
    }
    
    required convenience public init?(coder decoder: NSCoder) {
        guard let createDate = decoder.decodeObject(forKey: "createDate") as? Date,
            let entity = NSEntityDescription.entity(forEntityName: "WorkSlip", in: NSManagedObjectContext.mr_default())
            else { return nil }
        
        self.init(entity: entity, insertInto: nil)
        self.createDate = createDate as NSDate
    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(self.createDate, forKey: "createDate")
    }
    
    func exportWorkSummary() -> String {
        var buffer = ""
        
        if let safeStatus = status, let safeJira = jiraIdentifier, let safeSprint = sprint, let desc = about {
            if let statusValue = WorkSlipStatus(rawValue: safeStatus) {
                buffer = statusValue.exportWorkSummary(jira: safeJira, sprint: safeSprint, desc: desc);
            }
            
            if didPullRequest == true {
                buffer = buffer.appending("Did pull request for ticket \(safeJira)\n")
            }
        }
        return buffer
    }
    
    func clone(ctx: NSManagedObjectContext, slip: TimeSlip) {
        guard let clonedWorkSlip = WorkSlip.mr_createEntity(in: ctx) else { return }
        
        self.copyAttributes(to: clonedWorkSlip)
        slip.addToWorkSlips(clonedWorkSlip)
    }
}
