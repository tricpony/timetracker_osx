//
//  CodeReview+CoreDataClass.swift
//  
//
//  Created by aarthur on 2/15/19.
//
//

import Foundation
import CoreData

@objc(CodeReview)
public class CodeReview: BaseManagedObject, NSCoding, OutlineViewArchivable {
    lazy var archivableDate: Date = {
        guard let date = self.createDate else { return Date() }
        return date as Date
    }()
    
    func archivableMatchesObject(_ value: OutlineViewArchivable) -> Bool {
        return archivableDate == value.archivableDate
    }
    
    required convenience public init?(coder decoder: NSCoder) {
        guard let createDate = decoder.decodeObject(forKey: "createDate") as? Date,
            let entity = NSEntityDescription.entity(forEntityName: "CodeReview", in: NSManagedObjectContext.mr_default())
            else { return nil }
        
        self.init(entity: entity, insertInto: nil)
        self.createDate = createDate as NSDate
    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(self.createDate, forKey: "createDate")
    }

    func exportCodeReviewSummary(as type: ReportType = .csv) -> String {
        guard let jira = jiraIdentifier else { return "" }
        switch type {
        case .csv:
            return "Performed code review for \(jira) "
        case .imt:
            return "Performed code review for \(jira)\n"
        }

    }
    
    func clone(ctx: NSManagedObjectContext, slip: TimeSlip) {
        guard let clonedCodeReview = CodeReview.mr_createEntity(in: ctx) else { return }
        
        self.copyAttributes(to: clonedCodeReview)
        slip.addToCodeReviews(clonedCodeReview)
    }
}
