//
//  Enterprise+CoreDataClass.swift
//  
//
//  Created by aarthur on 2/2/19.
//
//

import Foundation
import CoreData

@objc(Enterprise)
public class Enterprise: BaseManagedObject, NSCoding, OutlineViewArchivable {
    lazy var archivableDate: Date = {
        guard let date = self.createDate else { return Date() }
        return date as Date
    }()

    // MARK: - ConfigurationKeyValueCompliant
    
    override var createNextImtIdentifier: String {
        let ctx = NSManagedObjectContext.mr_default()
        let className = self.className
        guard let config = CoreDataUtility.fetchConfiguration(in: ctx, matcingClassName: className) else {
            fatalError()
        }
        return "z\(config.nextIMT_ID)"
    }

    // MARK: - Importable
 
    @objc override var importableName: String? {
        get {
            return name
        }
        
        set (newValue) {
            name = newValue
        }
    }

    func archivableMatchesObject(_ value: OutlineViewArchivable) -> Bool {
        return archivableDate == value.archivableDate
    }

    required convenience public init?(coder decoder: NSCoder) {
        guard let createDate = decoder.decodeObject(forKey: "createDate") as? Date,
            let entity = NSEntityDescription.entity(forEntityName: "Enterprise", in: NSManagedObjectContext.mr_default())
            else { return nil }
        
        self.init(entity: entity, insertInto: nil)
        self.createDate = createDate as NSDate
    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(self.createDate, forKey: "createDate")
    }

    class func enterpriseObject(in ctx: NSManagedObjectContext) -> Enterprise? {
    
        var me: Enterprise? = nil
        
        if CoreDataUtility.fetchEnterpriseCount(in: ctx) == 0 {
            me = Enterprise.mr_createEntity(in: ctx)
        }else{
            me = CoreDataUtility.fetchEnterprise(in: ctx)
        }
        return me
    }
    
    // MARK: - IMT Export
    
    override func exportConfig(_ config: Configuration, invoice: Invoice) -> String {
        let id_substitution = "$IDENTIFIER$"
        guard let template = config.template else { return "" }

        guard let id = self.createNextImtIdentifier as String? else { return "" }
        guard let substitution = config.templateSubstitution else { return "" }
        guard let key = config.valueKeyPath else { return "" }
        guard let value = self.value(forKeyPath: key) as? String else { return "" }
        imtIdentifier = id

        return (template.findAndReplacement(of: [id_substitution:id, substitution:value]))
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//
/**
 This is here to help the outline view & the tree controller to work together
 These keys are set in the attributes inspector of the NSTreeController object
 **/
extension Enterprise: OutlineViewable {
    var isLeaf: Bool {
        let clientsSet = clients as! Set<Client>
        let projects = clientsSet.compactMap({ $0.projects })
        return projects.count == 0
    }
    
    var outlineCount: Int {
        let clientsSet = clients as! Set<Client>
        let projects = clientsSet.compactMap({ $0.projects })
        return projects.count
    }
    
    var outlineChildren: [OutlineViewable] {
        var projectsArray = [OutlineViewable]()
        for nextClient in clients as! Set<Client> {
            for nextPatient in nextClient.projects as! Set<Project> {
                projectsArray.append(nextPatient)
            }
        }
        return projectsArray
    }
}
//
//////////////////////////////////////////////////////////////////////////////////////////////////


