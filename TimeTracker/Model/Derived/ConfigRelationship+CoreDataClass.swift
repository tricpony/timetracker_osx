//
//  ConfigRelationship+CoreDataClass.swift
//  
//
//  Created by aarthur on 2/19/19.
//
//

import Foundation
import CoreData

@objc(ConfigRelationship)
public class ConfigRelationship: BaseManagedObject, Decodable {

    private enum CodingKeys: String, CodingKey {
        case cardinality
        case hidden
        case mapsTo
        case name
        case templateSubstitution
        case valueKeyPath
        case rootObjectKeyPath
    }
    
    public required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoContextKey = CodingUserInfoKey.context,
            let managedObjectContext = decoder.userInfo[codingUserInfoContextKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "ConfigRelationship", in: managedObjectContext) else {
                fatalError("Failed to decode Block")
        }
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            self.init(entity: entity, insertInto: managedObjectContext)
            self.cardinality = try container.decode(String.self, forKey: .cardinality)
            self.hidden = try container.decode(Bool.self, forKey: .hidden)
            self.mapsTo = try container.decode(String.self, forKey: .mapsTo)
            self.name = try container.decode(String.self, forKey: .name)
            self.templateSubstitution = try container.decode(String.self, forKey: .templateSubstitution)
            self.valueKeyPath = try container.decode(String.self, forKey: .valueKeyPath)
            self.rootObjectKeyPath = try container.decode(String.self, forKey: .rootObjectKeyPath)
        }
        catch {
            print(error)
        }
    }
}
