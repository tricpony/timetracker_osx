//
//  ConfigVersion+CoreDataClass.swift
//  
//
//  Created by aarthur on 2/19/19.
//
//

import Foundation
import CoreData

extension CodingUserInfoKey {
    static let context = CodingUserInfoKey(rawValue: "context")
}

@objc(ConfigVersion)
public class ConfigVersion: BaseManagedObject, Decodable {

    private enum CodingKeys: String, CodingKey {
        case version
    }
    
    public required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoContextKey = CodingUserInfoKey.context,
            let managedObjectContext = decoder.userInfo[codingUserInfoContextKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "ConfigVersion", in: managedObjectContext) else {
                fatalError("Failed to decode Block")
        }
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.init(entity: entity, insertInto: managedObjectContext)
        self.version = try container.decode(String.self, forKey: .version)
    }
}
