//
//  Configuration+CoreDataClass.swift
//  
//
//  Created by aarthur on 2/19/19.
//
//

import Foundation
import CoreData

@objc(Configuration)
public class Configuration: BaseManagedObject, Decodable, Comparable {
    
    private enum CodingKeys: String, CodingKey {
        case durationTemplateSubstitution
        case durationValueKeyPath
        case nextIMT_ID = "id_seed"
        case identifier = "Class"
        case objectType
        case startingDateTemplateSubstitution
        case startingDateValueKeyPath
        case template
        case templateSubstitution
        case valueKeyPath
        case rootObjectKeyPath
        case order
    }
    
    public required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoContextKey = CodingUserInfoKey.context,
            let managedObjectContext = decoder.userInfo[codingUserInfoContextKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "Configuration", in: managedObjectContext) else {
                fatalError("Failed to decode")
        }
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            self.init(entity: entity, insertInto: managedObjectContext)
            self.template = try container.decode(String.self, forKey: .template)
            self.rootObjectKeyPath = try container.decode(String.self, forKey: .rootObjectKeyPath)
            self.nextIMT_ID = try container.decode(Int32.self, forKey: .nextIMT_ID)
            self.identifier = try container.decode(String.self, forKey: .identifier)
            self.objectType = try container.decode(String.self, forKey: .objectType)
            self.templateSubstitution = try container.decode(String.self, forKey: .templateSubstitution)
            self.valueKeyPath = try container.decode(String.self, forKey: .valueKeyPath)
            self.order = try container.decode(Int16.self, forKey: .order)
        }
        catch {
            print(error)
        }

        if identifier == "TimeSlip" {
            self.durationTemplateSubstitution = try container.decode(String.self, forKey: .durationTemplateSubstitution)
            self.durationValueKeyPath = try container.decode(String.self, forKey: .durationValueKeyPath)
            self.startingDateTemplateSubstitution = try container.decode(String.self, forKey: .startingDateTemplateSubstitution)
            self.startingDateValueKeyPath = try container.decode(String.self, forKey: .startingDateValueKeyPath)
        }
    }
    
    func nextID() -> Int32 {
        return nextIMT_ID + 2
    }
    
    // MARK: - Comparable
    
    static func == (lhs: Configuration, rhs: Configuration) -> Bool {
        return (lhs.order == rhs.order)
    }
    
    public static func < (lhs: Configuration, rhs: Configuration) -> Bool {
        return lhs.order < rhs.order
    }
}
